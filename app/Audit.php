<?php

namespace App;
use App\Point;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $fillable = [
      'tasks','likes','referrals'
    ];
}
