<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashout extends Model
{
    protected $fillable = [
      'amount','user_id','transaction_id','account_num','account_name'
    ];
}
