<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Account;
use App\Point;
use App\Models\TaskUser;

class AccountController extends Controller
{
  public function store(Account $account,Request $request)
  {
     $user_id = auth()->user()->id;
      $attributes = $request->validate([
        'number' => 'required | min:10',
        'name' => 'required | min:12'
      ]);
       $accountType = $request->type;
       $bank = $request->bank;
       if($accountType == 'Account Type'){
         $error[] = 'Please select an account type';
       }
       if($bank == 'Select Bank'){
         $error[] = 'Please select a bank';
       }
       if(empty($error)){
         $account::create([
           'user_id' => $user_id,
           'account_num' => $request->number,
           'account_type' => $accountType,
           'account_name' => $request->name,
           'bank' => $request->bank
         ]);
         return back();
       }
       return redirect()->back()->with(['error' => $error]);
  }
  public function update($id,Request $request)
  {
    $account = Account::findOrFail($id);
    $attributes = $request->validate([
      'number' => 'required | min:10',
      'name' => 'required | min:12'
    ]);
     $accountType = $request->type;
     $bank = $request->bank;
       $account->update([
         'account_num' => $request->number,
         'account_type' => $accountType,
         'account_name' => $request->name,
         'bank' => $request->bank
       ]);
       return back();
  }
  public function cashout($id,Point $point,TaskUser $taskUser,User $user,Request $request)
  {
    // check if its really the user before cashing out
    $user->authorization($id);
    $request->validate([
      'amount' => 'required'
    ]);

    $min = $point::first()->min;
    $max = $point::first()->max;
    $standard_point = $point::first()->points;
    $user = \App\Models\Earning::where('user_id',$id)->get();
    if(count($user) != 0){
      foreach($user as $point){
        $totalPoints = $point->points;
      }
      $user_details = \App\Models\User::where('id',$id)->get();
      foreach($user_details as $details){
        if($details->role_id != 5){
          $error[] = 'Only Premium users can Cash out';
        }
      }
      if($request->amount < $min){
        $error[] = 'Please you cannot withdraw less than the Minimum amount';
      }
      if($request->amount > $max){
        $error[] = 'Please you cannot withdraw more than the Maximum amount';
      }
      $amount_points = $request->amount * $standard_point;
      if($amount_points > $totalPoints){
        $error[] = 'Amount higher than wallet balance';
      }
      // check if user has an account
      if(User::find(auth()->user()->id)->account == null){
        $error[] = 'Please add your account details first';
      }
      if(empty($error)){
        \App\Models\Earning::where('user_id',$id)->update([
          'points' => $totalPoints - $amount_points,
          'earnings' => ($totalPoints-$amount_points)/$standard_point
        ]);
        $account_details = \App\Models\User::find($id)->account;
        \App\Cashout::create([
          'user_id' => $id,
          'transaction_id' => rand(1,100000),
          'account_name' => $account_details->account_name,
          'account_num' => $account_details->account_num,
          'amount' => $request->amount
        ]);
        return redirect()->back()->with(['success' => 'Cashout successful, an Admin will process your request shortly']);
      }
      else{
        return redirect()->back()->with(['user_error' => $error]);
      }

    }else{
      // error message
      $error[] = 'You only earn when you complete a task';
      return redirect()->back()->with(['user_error' => $error]);
    }
  }
}
