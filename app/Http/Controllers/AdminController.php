<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Token;
use App\Models\Task;
use App\Models\TaskUser;
use App\Models\Earnings;
use App\Point;
use App\Audit;
use App\Cashout;

use Session;

class AdminController extends Controller
{
  public function login(Request $request){

      if($request->isMethod('post')){
        $data = $request->input();
        if(Auth::attempt(['email'=>$data['email'],'password'=>$data['password'],'is_admin'=>'1'])){
        //  echo "success"; die;
        Session::put('adminSession',$data['email']);
        return redirect('/inhouse/admin');
        }else{
          //echo "Failed"; die;
          return redirect('/inhouse')->with('flash_message_error','Invalid User name or Password');
        }
      }

    return view('admin.login');
    }
    public function admin(){
       if (Session::has('adminSession')){
         // go to controll
       }else{
         return redirect('/inhouse')->with('flash_message_error_access','Access Denied! ');
       }
      return view('admin.control');
    }
    public function logout(){
      Session::flush();
        return redirect('/inhouse')->with('flash_message_success','You have Logged out');

    }
    public function pwreset(){
      return view ('admin.password');

    }

    public function token(){
      return view ('admin.token');
    }
    public function generate_token(Token $token){
      $min = 1000;
      $max = 100000;
      $token_key = rand($min,$max);
      $token::create([
        'token' => $token_key
      ]);
      return redirect()->back()->with(['token' => $token_key]);
    }
    public function users()
    {
      $users = \App\User::all();
      return view('admin.users',['users' => $users]);
    }
    public function createTask($id)
    {
      return view('admin.createTask',['id' =>$id]);
    }
    // public function assignTask($id,Request $request,Task $task)
    // {
    //   $request->validate([
    //     'title' => 'required | min:12',
    //     'instruction' => 'required | min:30',
    //     'points' => 'required'
    //   ]);
    //   $userTask = $task::where('user_id',$id)->get();
    //   if(count($userTask) == '0'){
    //     $task::create([
    //       'title' => $request->title,
    //       'instruction' => $request->instruction,
    //       'user_id' => $id,
    //       'reward_points' => $request->points
    //     ]);
    //     return redirect()->back()->with(['success' => 'Task assigned successfully']);
    //   }
    //   else{
    //     $task::where('user_id',$id)->update([
    //       'title' => $request->title,
    //       'instruction' => $request->instruction,
    //       'reward_points' => $request->points,
    //       'tasks_pending_approval' => 1,
    //       'tasks_approved' => 0
    //     ]);
    //     return redirect()->back()->with(['success' => 'Task assigned successfully']);
    //   }
    // }

    public function taskSettings()
    {
        return view('admin.task_settings');
    }

    public function set(Point $point,Request $request)
    {
      $point::create($request->validate([
        'points' => 'required',
        'min' => 'required',
        'max' => 'required',
        'likes' => 'required',
        'likes_user' => 'required',
        'ref_user' => 'required',
        'subscription' => 'required'
      ]));
      return redirect()->back()->with(['success' => 'Settings has been set']);
    }
    public function updatePoint($id,Point $point,Request $request)
    {
       $point::findOrFail($id)->update(
         $request->validate([
          'points' => 'required',
          'min' => 'required',
          'max' => 'required',
          'likes' => 'required',
          'likes_user' => 'required',
          'ref_user' => 'required',
          'subscription' => 'required'
        ])
      );
       return redirect()->back()->with(['success' => 'Settings has been updated']);
    }
    public function approve()
    {
      $users = Task::where([
          ['tasks_pending_approval','1'],
          ['tasks_approved','<>','1']
        ])->get();
      return view('admin.approve',compact('users'));
    }
    public function approveTask($id,$task_id,Task $task,TaskUser $taskUser)
    {
      $standard = \App\Point::first()->points;
      $reward_points = $taskUser::where('task_id',$task_id)->get();
      foreach($reward_points as $reward_point){
        $reward = $reward_point->reward_points;
      }
      $task::where([
          ['user_id',$id],
          ['id',$task_id]
        ])->update([
        'tasks_approved' => 1
      ]);
      $taskUser::where('task_id',$task_id)->update([
        'status' => '1'
      ]);
      $this->creditUser($id,$reward);
      // update the total tasks earned by users in the audit table
      $this->updateTotalTasks($reward);

      return back();
    }
    public function creditUser($id,$points)
    {
      $earnings = \App\Models\Earning::where('user_id',$id)->get();
      $standard = \App\Point::first()->points;
      //check if the user exists in the earnings
      if(count($earnings) == 0){
        \App\Models\Earning::create([
          'user_id' => $id,
          'points' => $points,
           'earnings' => $points/$standard
        ]);
      }
      else{
          // if he exists update points and earnings
          foreach($earnings as $earn){
            $point = $earn->points;
            $user = $earn->earnings;
          }
          \App\Models\Earning::where('user_id',$id)->update([
            'points' => $point + $points,
            'earnings' => $user+($points/$standard)
          ]);
      }
    }

    public function cashoutView()
    {
      return view('admin.cashout');
    }
    public function cashout($transaction_id)
    {
      Cashout::where('transaction_id',$transaction_id)->update([
        'status' => 1
      ]);
      // record the cashout in the audit table
      $amount = Cashout::where('transaction_id',$transaction_id)->first()->amount;
      $this->updateCashout($amount);
      return back();
    }
    // update the cashout value in the audit table
    public function updateCashout($amount)
    {
      // update the audits table
      $audit = Audit::first();
      if($audit == null){
        $this->updateCashoutIfNull($amount);
      }
      else{
        $this->updateCashoutIfNotNull($amount);
      }
    }

    public function audit_view()
    {
      // returns the view of updated results from the audits of the system
      $audit = Audit::first();
      return view('admin.audit',compact('audit'));
    }
    // audit to record total point from premium users tasks in the system
    public function updateTotalTasks($total_tasks)
    {
      // get the standard point for conversion
      $standard_point = Point::first()->points;
      // convert the total like points to money
      $total_tasks_points = $total_tasks/$standard_point;
      $audit = Audit::first();
      if($audit == null){
        $this->updateIfNull($total_tasks_points);
      }
      else{
        $this->updateIfNotNull($total_tasks_points);
      }
    }
    public function updateIfNull($reward)
    {
      $id = Audit::first()->id;
      Audit::where('id',$id)->update([
        'tasks' => $reward
      ]);
    }
    public function updateIfNotNull($reward)
    {
      $id = Audit::first()->id;
      $former_point = Audit::first()->tasks;
      Audit::where('id',$id)->update([
        'tasks' => $reward + $former_point
      ]);
    }
    public function updateCashoutIfNull($amount)
    {
      $id = Audit::first()->id;
      Audit::where('id',$id)->update([
        'cashouts' => $amount
      ]);
    }
    public function updateCashoutIfNotNull($amount)
    {
      $id = Audit::first()->id;
      $former_point = Audit::first()->cashouts;
      Audit::where('id',$id)->update([
        'cashouts' => $amount + $former_point
      ]);
    }
}
