<?php

namespace App\Http\Controllers;

use App\Models\Earning;
use Illuminate\Http\Request;
// use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Account;
use App\Models\Task;
use App\Models\TaskUser;
use App\Http\Controllers\HomeController;

class EarningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {




    if(Auth::user('role_id'=='5')){
      $taskUsers = TaskUser::where('user_id', Auth::user()->id)->get();
      $tasks = Task::all();

      return view('wallet.index')
          ->with('taskUsers', $taskUsers)
          ->with('tasks', $tasks);
    }else{
      //echo "Failed"; die;
      return redirect('/home')->with('flash_message_error','Invalid User name or Password');
    }
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Earning  $earning
     * @return \Illuminate\Http\Response
     */
    public function show(Earning $earning)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Earning  $earning
     * @return \Illuminate\Http\Response
     */
    public function edit(Earning $earning)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Earning  $earning
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Earning  $earning
     * @return \Illuminate\Http\Response
     */
    public function destroy(Earning $earning)
    {
        //
    }
}
