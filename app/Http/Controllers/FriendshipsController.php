<?php

namespace App\Http\Controllers;

use App\Models\Friendships;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class FriendshipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $friends = Friendships::all();

       return view('users.friendlist')->with('friends', $friends);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $requests = Friendships::where('user_requested', '=' , Auth::id())
            ->where('status', '=', 'pending')
            ->get();

        return view('users.requests')->with('requests', $requests);
    }

    public function getUser($id)
    {
        $user = User::find($id);

        return $user;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $friend = new Friendships;

        $friend->requester = Auth::id();
        $friend->user_requested = $id;
        $friend->status = 'pending';

        if (Auth::id() == $id)
        {
            return back()->with('status', 'You cannot send Friend Request to Yourself!');
        }else{
            $friend->save();

            return back()->with('status', 'Friend Request Sent!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Friendships  $friendships
     * @return \Illuminate\Http\Response
     */
    public function show(Friendships $friendships)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Friendships  $friendships
     * @return \Illuminate\Http\Response
     */
    public function edit(Friendships $friendships)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Friendships  $friendships
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $friend = Friendships::find($id);

        $friend->status = 'friends';

        $friend->update();

        return back()->with('status', 'Friend Request Accepted!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Friendships  $friendships
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $friend = Friendships::find($id);
        $friend->delete();
        return back()->with('status', 'Friend Removed!');
    }
}
