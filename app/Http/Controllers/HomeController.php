<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Token;
use App\Point;
use App\Audit;
use App\Models\Earning;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::orderby('id', 'desc')->paginate(10);
        return view('home')->with('posts', $post);
    }
    public function upgrade(Request $request,Token $token_key,User $user)
    {
        $request->validate([
          'code' => 'required'
        ]);
        $code = intval($request->code);
        $token = \App\Token::where('token',$code)->get();
        foreach($token as $tokens){
          if($tokens->is_used == '1'){
            $error[] = 'Token already used';
          }
        }
         if(count($token) !== 0 && empty($error)){
           $token_key->where('token',$code)->update([
             'is_used' => '1'
           ]);
           $user->where('id',
           auth()->user()->id )->update([
             'role_id' => '5'
           ]);
           $success = 'Congratulations, you are now a premium user';
           // Let the premium user that referred the user earn
           $user = User::findOrFail(auth()->user()->id);
           // standard point used in conversion
           $standard = Point::first()->points;
           $ref_point = Point::first()->ref_user;
           $credit_user = Earning::where('user_id',$user->referred_by)->first();
            if($credit_user != null){
              $newpoint = $credit_user->points+$ref_point;
              $credit_user->earnings = ($newpoint)/$standard;
              $credit_user->points = $newpoint;
              $credit_user->save();
            }
            // update total earned from referrals in the audit table
            $this->updateTotalRefs($ref_point);
            // update total earned from subscription in the audits table
            $this->updateSubscription();

           return redirect()->back()->with(['success' => $success]);
         }
         else{
           $error[] = 'Invalid Token';
           }
           return redirect()->back()->with(['error' => $error]);
         }

         // audit to record total point from premium users likes in the system
         public function updateTotalRefs($total_refs)
         {
           // get the standard point for conversion
           $standard_point = Point::first()->points;
           // convert the total ref points to money
           $total_ref_points = $total_refs/$standard_point;
           $audit = Audit::first();
           if($audit == null){
             $this->createIfNull($total_ref_points);
           }
           else{
             $this->updateIfNotNull($total_ref_points);
           }
         }
         // update total subscription in audits table
         public function updateSubscription()
         {
           $audit = Audit::first();
           $subscription_fee = Point::first()->subscription;
           if($audit == null){
             $this->createSubscriptionIfNull($subscription_fee);
           }
           else{
             $this->updateSubscriptionIfNotNull($subscription_fee);
           }
         }

         public function createIfNull($reward)
         {
           Audit::create([
             'referrals' => $reward
           ]);
         }
         public function updateIfNotNull($reward)
         {
           $id = Audit::first()->id;
           $former_point = Audit::first()->referrals;
           Audit::where('id',$id)->update([
             'referrals' => $reward + $former_point
           ]);
         }

         public function updateSubscriptionIfNotNull($subscription)
         {
           $id = Audit::first()->id;
           $former_point = Audit::first()->subscriptions;
           Audit::where('id',$id)->update([
             'subscriptions' => $subscription + $former_point
           ]);
         }
         public function createSubscriptionIfNull($subscription)
         {
           Audit::create([
             'subscriptions' => $subscription
           ]);
         }
    }
