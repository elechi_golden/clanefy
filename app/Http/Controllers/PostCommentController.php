<?php

namespace App\Http\Controllers;

use App\Models\PostComment;
use App\Models\PostsNotification;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new PostComment;

        $comment->user_id = Auth::id();
        $comment->post_id = $request->input('post_id');
        $comment->comment = $request->input('comment');
        
        if($comment->save()){


            $post = Post::where('id', $comment->post_id)->first();
            $user_notified = $post->user_id;

            if($user_notified !== $comment->user_id){

                $user_name = Auth::user()->name;

                $notification = new PostsNotification;
                $notification->user_notified = $user_notified;
                $notification->user_sent_from = Auth::id();
                $notification->post_id = $comment->post_id;
                $notification->comment_id = $comment->id;
                $notification->notification = ''.$user_name.' Commented on your post';
                $notification->url = "/post/$comment->post_id";

                $notification->save();
            }
        }


        return back()->with('status', 'Comment Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostComment  $postComment
     * @return \Illuminate\Http\Response
     */
    public function show(PostComment $postComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PostComment  $postComment
     * @return \Illuminate\Http\Response
     */
    public function edit(PostComment $postComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PostComment  $postComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostComment $postComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PostComment  $postComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostComment $postComment)
    {
        //
    }
}
