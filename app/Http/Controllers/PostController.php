<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::orderby('id', 'desc')->paginate(10);
        //return view('')->with('posts', $post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'post' => 'required',
           'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|nullable|max:1024'
        ]);

        if($request->hasFile('picture'))
        {
            $file = $request->file('picture')->getClientOriginalName();
            $fileName = time().'_'.$file;

            $path = $request->file('picture')->storeAs('public/images', $fileName);
        }

         $post = new Post();

        $post->user_id = Auth::id();
        $post->post_topic = $request->input('post_topic');
        $post->post = $request->input('post');
        if(!empty($fileName)){
            $post->picture = $fileName;
        }

        $post->save();
        return back()->with('status', 'Post Published!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        //return $comments;
        //return $post;
        return view('post.post')->with('posts', $post);
    }
    public function getRandomPost() {
    $post = Post::inRandomOrder()->first();
    return view('');
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        // return $comments;
        return view('')->with('posts', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'post' => 'required',
            'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|nullable|max:1024'
        ]);

        if($request->hasFile('picture'))
        {
            $file = $request->file('picture')->getClientOriginalName();
            $fileName = time().'_'.$file;

            $path = $request->file('picture')->storeAs('public/images', $fileName);
        }

        $post = Post::find($id);

        $post->post_topic = $request->input('post_topic');
        $post->post = $request->input('post');
        if(!empty($fileName)){
            $post->picture = $fileName;
        }
        $post->updated_at = NOW();
        $post->save();
        return back()->with('status', 'Post Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(!empty($post->picture)){
            Storage::delete('public/images/'.$post->picture);
        }
        $post->delete();
        return back()->with('success', 'Post Deleted');
    }



}
