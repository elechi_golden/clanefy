<?php

namespace App\Http\Controllers;

use App\Models\PostLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Earning;
use App\Models\User;
use App\Point;
use App\Models\Post;
use App\Audit;


class PostLikeController extends Controller
{
  public $total_like_points = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($post_id,Audit $audit)
    {
        $checkLike = PostLike::where('user_id', '=' , Auth::id())
            ->where('post_id', '=', $post_id)
            ->get();
            $user = User::where('id',auth()->user()->id)->get();
            foreach ($user as $users) {
              $role_id = $users->role_id;
            }

        if(count($checkLike) == 0){
            $like = new PostLike;

            $like->user_id = Auth::id();
            $like->post_id = $post_id;

            if($like->save()){
                $posts = Post::where('id', $post_id)->first();
                $this->creditLikedUser($posts,$role_id);
                // update the total like points in the audits table
                $this->updateTotalLikes($this->total_like_points);
            }
           return back();
        }else
        {
            return back()->with('status', 'Already Liked!');
        }
    }
    // audit to record total point from premium users likes in the system
    public function updateTotalLikes($total_like_points)
    {
      // get the standard point for conversion
      $standard_point = Point::first()->points;
      // convert the total like points to money
      $total_like_points = $total_like_points/$standard_point;
      $audit = Audit::first();
      if($audit == null){
        Audit::create([
          'likes' => $total_like_points
        ]);
      }
      else{
        $former_point = $audit::first()->likes;
        if($former_point == null){
          $this->createIfNull($total_like_points);
        }
        else{
          $this->updateIfNotNull($total_like_points);
        }
      }
    }
    public function createIfNull($reward)
    {
      Audit::create([
        'likes' => $reward
      ]);
    }
    public function updateIfNotNull($reward)
    {
      $id = Audit::first()->id;
      $former_point = Audit::first()->likes;
      Audit::where('id',$id)->update([
        'likes' => $reward + $former_point
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function show(PostLike $postLike)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function edit(PostLike $postLike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostLike $postLike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PostLike  $postLike
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostLike $postLike)
    {
        //
    }

    public function creditLikedUser($posts,$role_id)
    {
      if(($posts->user_id !== Auth::id()) && ($role_id == 5)){

          $earnings = Earning::where('user_id', $posts->user_id)->first();
          $like_point = Point::first()->likes;
          $standard = Point::first()->points;

          if(empty($earnings)){
              $newearnings = new Earning;
              $newearnings->user_id = $posts->user_id;
              $newearnings->points = $like_point;
              $newearnings->earnings = ($like_point)/$standard;
              // getting the total amount of likes earned from premium users
              $this->total_like_points = $this->total_like_points + $like_point;

              $newearnings->save();
          }else{
              $newpoint = $earnings->points+$like_point;
              $earnings->earnings = ($newpoint)/$standard;
              $earnings->points = $newpoint;
              // getting the total amount of likes earned from premium users
              $this->total_like_points = $this->total_like_points + $like_point;

              $earnings->save();
          }
         $this->creditUserWhoLikes($posts,$role_id);
      }
    }

    public function creditUserWhoLikes($posts,$role_id)
    {
      if($posts->user_id !== Auth::id()){
        if(!empty(auth()->user()->id) && ($role_id == 5)){
          $earnings = Earning::where('user_id', auth()->user()->id)->first();
          $like_point = Point::first()->likes_user;
          $standard = Point::first()->points;

          if(empty($earnings)){
            $newearnings = new Earning;
            $newearnings->user_id = auth()->user()->id;
            $newearnings->points = $like_point;
            $newearnings->earnings = ($like_point)/$standard;
            // getting the total amount of likes earned from premium users
            $this->total_like_points = $this->total_like_points + $like_point;

            $newearnings->save();
          }else{
            $newpoint = $earnings->points+$like_point;
            $earnings->points = $newpoint;
            $earnings->earnings = ($newpoint)/$standard;
            // getting the total amount of likes earned from premium users
            $this->total_like_points = $this->total_like_points + $like_point;

            $earnings->save();
          }
        }
      }
    }
}
