<?php

namespace App\Http\Controllers;

use App\Models\PostsNotification;
use Illuminate\Http\Request;

class PostsNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notification = PostsNotification::where('user_notified', Auth::id())
        ->where('status', 'unread')
        ->all();

        return view('')->with('notifications', $notification);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostsNotification  $postsNotification
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = PostsNotification::where('id', $id)->first();

        $notification->status = 'read';

        $notification->save();

        return redirect($notification->url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostsNotification  $postsNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(PostsNotification $postsNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostsNotification  $postsNotification
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $notification = PostsNotification::find($id);

        $notification->status = 'read';

        $notification->update();

        return redirect($notification->url); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostsNotification  $postsNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostsNotification $postsNotification)
    {
        //
    }
}
