<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRefCategoryRequest;
use App\Http\Requests\UpdateRefCategoryRequest;
use App\Repositories\RefCategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Auth;
use Cookie;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\RefCategory;
use App\Models\User;

class RefCategoryController extends AppBaseController
{
    /** @var  RefCategoryRepository */
    private $refCategoryRepository;

    public function __construct(RefCategoryRepository $refCategoryRepo)
    {
        $this->refCategoryRepository = $refCategoryRepo;
    }

    /**
     * Display a listing of the RefCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, User $user)
    {
        $this->refCategoryRepository->pushCriteria(new RequestCriteria($request));
        $refCategories = $this->refCategoryRepository->getUserRef();
        $ref_users = $user->ref_users();
        $referred_by = $user->referredBy();
        return view('ref_categories.index',compact('refCategories','ref_users','referred_by'));
    }

    /**
     * Show the form for creating a new RefCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('ref_categories.create');
    }

    /**
     * Store a newly created RefCategory in storage.
     *
     * @param CreateRefCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateRefCategoryRequest $request)
    {
      // $requests used to instantiate the Request class
        $input = $request->all();
        $input['user_id']= Auth::user()->id;
        $user = auth()->user();
        $role_id = $user->role_id;
        if($role_id == '5'){
          $refCategory = $this->refCategoryRepository->create($input);
          Flash::success('Referrel link successfully Created.');
        }
        else{
          Flash::error('You need to upgrade to a premium user');
        }


        return redirect(route('refCategories.index'));
    }

    /**
     * Display the specified RefCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $refCategory = $this->refCategoryRepository->findWithoutFail($id);

        if (empty($refCategory)) {
            Flash::error('Referrer link  not found');

            return redirect(route('refCategories.index'));
        }

        return view('ref_categories.show')->with('refCategory', $refCategory);
    }
    public function refs($user_id = null, $ref_category_id = null,Request $request){
      // check if the user_id and refer_category exists
      $user = User::findOrFail($user_id);
      $user_ref = RefCategory::where('id', '=',$ref_category_id)->where('user_id','=',$user->id)->first();
      if($user_ref == Null){
        abort(403);
      }


        $ref_category_id = $user_ref->id;
        $refCategory = $user_ref;
        // only users that sign up with premium users link can make the premium user earn
        // make the link redirect to a registeration page
        // the redirected new user should have the premium users id tagged to his
        // talking about the visits it can keep counting
        // signups should count when referred users signup
        // admin should set how much is to be earned from ref_users

          RefCategory::where('id', $ref_category_id)->update([
            'ref_visit' => $refCategory->ref_visit +1,
          ]);

        //cookies to track Visits
      return redirect(route('register'))
      ->cookie('ref_user_id', $user_id, 1)
      ->cookie('ref_category_id', $ref_category_id, 1);


    }

    /**
     * Show the form for editing the specified RefCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $refCategory = $this->refCategoryRepository->findWithoutFail($id);

        if (empty($refCategory)) {
            Flash::error('Ref Category not found');

            return redirect(route('refCategories.index'));
        }

        return view('ref_categories.edit')->with('refCategory', $refCategory);
    }

    /**
     * Update the specified RefCategory in storage.
     *
     * @param  int              $id
     * @param UpdateRefCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRefCategoryRequest $request)
    {
        $refCategory = $this->refCategoryRepository->findWithoutFail($id);

        if (empty($refCategory)) {
            Flash::error('Ref Category not found');

            return redirect(route('refCategories.index'));
        }

        $refCategory = $this->refCategoryRepository->update($request->all(), $id);

        Flash::success('Ref Category updated successfully.');

        return redirect(route('refCategories.index'));
    }

    /**
     * Remove the specified RefCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $refCategory = $this->refCategoryRepository->findWithoutFail($id);

        if (empty($refCategory)) {
            Flash::error('Ref Category not found');

            return redirect(route('refCategories.index'));
        }

        $this->refCategoryRepository->delete($id);

        Flash::success('Ref Category deleted successfully.');

        return redirect(route('refCategories.index'));
    }
}
