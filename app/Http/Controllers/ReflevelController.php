<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReflevelRequest;
use App\Http\Requests\UpdateReflevelRequest;
use App\Repositories\ReflevelRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReflevelController extends AppBaseController
{
    /** @var  ReflevelRepository */
    private $reflevelRepository;

    public function __construct(ReflevelRepository $reflevelRepo)
    {
        $this->reflevelRepository = $reflevelRepo;
    }

    /**
     * Display a listing of the Reflevel.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->reflevelRepository->pushCriteria(new RequestCriteria($request));
        $reflevels = $this->reflevelRepository->all();

        return view('reflevels.index')
            ->with('reflevels', $reflevels);
    }

    /**
     * Show the form for creating a new Reflevel.
     *
     * @return Response
     */
    public function create()
    {
        return view('reflevels.create');
    }

    /**
     * Store a newly created Reflevel in storage.
     *
     * @param CreateReflevelRequest $request
     *
     * @return Response
     */
    public function store(CreateReflevelRequest $request)
    {
        $input = $request->all();

        $reflevel = $this->reflevelRepository->create($input);

        Flash::success('Reflevel saved successfully.');

        return redirect(route('reflevels.index'));
    }

    /**
     * Display the specified Reflevel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reflevel = $this->reflevelRepository->findWithoutFail($id);

        if (empty($reflevel)) {
            Flash::error('Reflevel not found');

            return redirect(route('reflevels.index'));
        }

        return view('reflevels.show')->with('reflevel', $reflevel);
    }

    /**
     * Show the form for editing the specified Reflevel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reflevel = $this->reflevelRepository->findWithoutFail($id);

        if (empty($reflevel)) {
            Flash::error('Reflevel not found');

            return redirect(route('reflevels.index'));
        }

        return view('reflevels.edit')->with('reflevel', $reflevel);
    }

    /**
     * Update the specified Reflevel in storage.
     *
     * @param  int              $id
     * @param UpdateReflevelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReflevelRequest $request)
    {
        $reflevel = $this->reflevelRepository->findWithoutFail($id);

        if (empty($reflevel)) {
            Flash::error('Reflevel not found');

            return redirect(route('reflevels.index'));
        }

        $reflevel = $this->reflevelRepository->update($request->all(), $id);

        Flash::success('Reflevel updated successfully.');

        return redirect(route('reflevels.index'));
    }

    /**
     * Remove the specified Reflevel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reflevel = $this->reflevelRepository->findWithoutFail($id);

        if (empty($reflevel)) {
            Flash::error('Reflevel not found');

            return redirect(route('reflevels.index'));
        }

        $this->reflevelRepository->delete($id);

        Flash::success('Reflevel deleted successfully.');

        return redirect(route('reflevels.index'));
    }
}
