<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friendships extends Model
{
    protected $fillable = ['requester', 'user_requested', 'status'];
}
