<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'post_topic', 'post', 'picture'];

    public function comments()
    {
        return $this->hasMany('App\Models\PostComment');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\PostLike');
    }
}
