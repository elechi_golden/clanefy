<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostsNotification extends Model
{
    protected $fillable = ['user_notified', 'user_sent_from', 'post_id', 'comment_id', 'notification', 'url'];
}
