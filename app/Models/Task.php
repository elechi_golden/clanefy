<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Task
 * @package App\Models
 * @version February 14, 2019, 6:04 pm UTC
 *
 * @property string title
 * @property string instruction
 * @property integer user_id
 * @property integer reward_points
 */
class Task extends Model
{
    use SoftDeletes;

    public $table = 'tasks';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    //this is a comment


    public $fillable = [
        'title',
        'instruction',
        'user_id',
        'reward_points'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'instruction' => 'string',
        'user_id' => 'integer',
        'reward_points' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');

    }


}
