<?php

namespace App\Models;



use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lexx\ChatMessenger\Traits\Messagable;


/**
 * Class User
 * @package App\Models
 * @version February 13, 2019, 10:23 pm UTC
 *
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property integer referred_by
 * @property integer no_of_refs
 * @property integer ref_level_id
 * @property integer role_id
 * @property string remember_token
 */



class User extends Model
{
    use SoftDeletes;


    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'avatar',
        'email_verified_at',
        'password',
        'bio',
        'dob',
        'phone',
        'gender',
        'address',
        'referred_by',
        'no_of_refs',
        'ref_level_id',
        'role_id',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'avatar' => 'string',
        'password' => 'string',
        'referred_by' => 'integer',
        'no_of_refs' => 'integer',
        'ref_level_id' => 'integer',
        'role_id' => 'integer',
        'remember_token' => 'string'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');

    }
    public function tasks()
    {
      return $this->hasMany('App\Models\Task');
    }

    public function account()
    {
      return $this->hasOne('App\Account','user_id');
    }
    public function authorization($id)
    {
      abort_if(auth()->user()->id != $id,403);
    }
    public function ref_users()
    {
      // get auth'd user referred users
      return User::where('referred_by',auth()->user()->id)->pluck('name','email');
    }
    // user was referred by who
    public function referredBy()
    {
      $id = auth()->user()->referred_by;
      return $id == null ? 'Nobody' : User::where('id',$id)->pluck('name');
    }

}
