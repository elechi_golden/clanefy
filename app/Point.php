<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $fillable = [
      'points','max','min','likes','likes_user','ref_user','subscription'
    ];
}
