<?php

namespace App\Repositories;

use App\Models\RefCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RefCategoryRepository
 * @package App\Repositories
 * @version February 14, 2019, 7:32 am UTC
 *
 * @method RefCategory findWithoutFail($id, $columns = ['*'])
 * @method RefCategory find($id, $columns = ['*'])
 * @method RefCategory first($columns = ['*'])
*/
class RefCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'ip_address',
        'user_id',
        'ref_visit',
        'ref_count'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RefCategory::class;
    }
    public function getUserRef()
    {
      return RefCategory::where('user_id',auth()->user()->id)->first();
    }
}
