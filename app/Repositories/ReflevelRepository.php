<?php

namespace App\Repositories;

use App\Models\Reflevel;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReflevelRepository
 * @package App\Repositories
 * @version February 14, 2019, 7:33 am UTC
 *
 * @method Reflevel findWithoutFail($id, $columns = ['*'])
 * @method Reflevel find($id, $columns = ['*'])
 * @method Reflevel first($columns = ['*'])
*/
class ReflevelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'reward',
        'congratulatory_message',
        'target_no_referrals',
        'point_per_referral'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reflevel::class;
    }
}
