<?php

namespace App\Traits;

use App\User;
use App\Models\Friendships;
use Illuminate\Support\Facades\Auth;

trait Friendable 
{
    public function hasFriend($id)
    {
        $hasFriend = Friendships::where('requester', '=' , Auth::id())
            ->where('user_requested', '=' , $id)
            ->where('status', '=', 'pending')
            ->first();

        if(!empty($hasFriend)){
        return 'requested';
        }

        $isFriend2 = Friendships::where('requester', '=' , $id)
            ->where('user_requested', '=' , Auth::id())
            ->where('status', '=', 'friends')
            ->first();

        if(!empty($isFriend2)){
        return 'friends';
        }

        $isFriend = Friendships::where('requester', '=' , Auth::id())
            ->where('user_requested', '=' , $id)
            ->where('status', '=', 'friends')
            ->first();
        
        if(!empty($isFriend)){
        return 'friends';
        }
    }
}