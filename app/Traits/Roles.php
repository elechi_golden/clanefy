<?php

namespace App\Traits;

use App\User;
use Illuminate\Support\Facades\Auth;

trait Roles 
{
    public function getRole($id)
    {
        $getRole = User::where('id', '=' , Auth::id())
            ->first();
        return $getRole->role;
    }
    
}