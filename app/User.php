<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Traits\Friendable;
use App\Traits\Roles;

use Lexx\ChatMessenger\Traits\Messagable;
use App\Models\Friendships;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use Friendable;
    use Roles;
   use Messagable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'referred_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

          public function comments()
      {
          return $this->hasMany(Comment::class);
      }

      public function tickets()
      {
          return $this->hasMany(Ticket::class);
      }

      public function friends()
      {
          return $this->hasMany(Friendships::class);
      }

}
