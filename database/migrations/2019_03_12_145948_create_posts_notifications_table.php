<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_notified');
            $table->integer('user_sent_from');
            $table->integer('post_id');
            $table->integer('comment_id')->nullable();
            $table->text('notification');
            $table->string('url');
            $table->string('status')->default('unread');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_notifications');
    }
}
