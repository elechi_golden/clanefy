@extends('layouts.admin')
@section('content')
<div class="mt-4 col-md-12 table-responsive" style="margin-top:5%">
  <p class="alert alert-info text-center"><strong>Ensure to check-in regularly to approve tasks</strong></p>
  <table class="table table-bordered">
    <thead>
      <th>#</th>
      <th>Task title</th>
      <th>Status</th>
    </thead>
    <tbody>
      @if(!empty($users))
        <?php $i = 1 ?>
        @foreach($users as $user)
        <tr>
          <td>{{ $i }}</td>
          <td>{{ $user->title }}</td>
          <td>
            <form action="/inhouse/approve/{{ $user->user_id }}/task/{{ $user->id }}" method="post">
              @method('PATCH')
              {{ csrf_field() }}
              <input type="submit"  class="btn btn-danger" name="" value="Pending">
            </form>
          </td>
        </tr>
        <?php $i++ ?>
        @endforeach
      @endif
      @if(count($users) == 0)
        <p class="alert alert-info text-center"><strong>No pending tasks awaiting approval</strong></p>
      @endif
    </tbody>
  </table>
</div>

@endsection
