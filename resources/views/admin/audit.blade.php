@extends('layouts.admin')
@section('content')
<div class="col-md-12 table-responsive" style="margin-top:5%">
  <h1>Audit Section</h1>
  <table class="table table-bordered">
    <thead>
      <th>Total earnings from completed tasks</th>
      <th>Total earnings from liking posts</th>
      <th>Total earnings from referrals</th>
      <th>Total earnings from Cashouts</th>
      <th>Total earnings from Subscriptions</th>
    </thead>
    <tbody>
      <td>{{ $audit->tasks }}</td>
      <td>{{ $audit->likes }}</td>
      <td>{{ $audit->referrals }}</td>
      <td>{{ $audit->cashouts }}</td>
      <td>{{ $audit->subscriptions }}</td>
    </tbody>
  </table>
  <div class="col-md-12 text-center">
    <div class="row">
      <div class="col-md-6">
        <h4>Net Profit :</h4>
      </div>
      <div class="col-md-6">
        <span>{{ $audit->subscriptions - $audit->cashouts }}</span>
      </div>
    </div>
  </div>
</div>

@endsection
