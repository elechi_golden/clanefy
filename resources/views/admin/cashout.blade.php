@extends('layouts.admin')
@section('content')
<div class="mt-4 col-md-12 table-responsive" style="margin-top:5%">
  <p class="alert alert-info text-center"><strong>Ensure to check-in regularly to approve cashouts</strong></p>
  <table class="table table-bordered">
    <thead>
      <th>#</th>
      <th>Transaction ID</th>
      <th>Account name</th>
      <th>Account num</th>
      <th>Amount</th>
      <th>Status</th>
    </thead>
    <tbody>
      <?php $user_cashouts = \App\Cashout::where('status',0)->get();$i = 1; ?>
      <?php if(count($user_cashouts) != 0): ?>
        <?php foreach($user_cashouts as $cashouts): ?>
          <tr>
            <td>{{ $i }}</td>
            <td>{{ $cashouts->transaction_id }}</td>
            <td>{{ $cashouts->account_name }}</td>
            <td>{{ $cashouts->account_num }}</td>
            <td>${{ $cashouts->amount }}</td>
            <td>
              <form action="/inhouse/cashout/{{ $cashouts->transaction_id }}" method="post">
                @method('PATCH')
                {{ csrf_field() }}
                <input type="submit" class="btn btn-success" name="" value="Cashout">
              </form>
            </td>
          </tr>
          <?php $i++ ?>
        <?php endforeach ?>
      <?php else: ?>
        <p class="alert alert-warning text-center"><strong>No pending Cashouts</strong></p>
      <?php endif ?>
    </tbody>
  </table>
</div>

@endsection
