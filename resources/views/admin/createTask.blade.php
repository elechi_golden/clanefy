@extends('layouts.admin')
@section('content')
<div class="mt-4 col-md-8 table-responsive" style="margin-top:5%">
  <?php $user = \App\User::findOrFail($id) ?>
  <h2>Assign a task to {{ $user->name }}</h2>
  @include('success')
  <form class="" action="/inhouse/assign_task/{{ $id }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="">Task Title</label>
      <input type="text" class="form-control" name="title" value="{{old('title')}}">
    </div>
    <div class="form-group">
      <label>Task Instruction</label>
      <textarea name="instruction" value="{{old('instruction')}}" class="form-control" rows="8" cols="80"></textarea>
    </div>
    <div class="form-group">
      <label for="">Reward Points</label>
      <input type="text" name="points" class="form-control" placeholder="please input the reward points" value="{{old('points')}}">
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-primary" name="" value="Create task">
    </div>
    @include('error')
  </form>
</div>

@endsection
