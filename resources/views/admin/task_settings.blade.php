@extends('layouts.admin')
@section('content')
<div class="col-md-8" style="margin-top:5%">
  <h2>Task settings</h2>
  <div class="alert alert-info">
    <p class="text-center">Please ensure to set the point conversion rate</p>
  </div>
  @include('success')
  <?php $points = \App\Point::first(); ?>
  <?php if(!empty($points)){
          $curl = '/inhouse/task_settings/update/'.$points->id;
          // dd($point['max']);
          $point = $points->points;
          $max = $points->max;
         $min = $points->min;
         $like = $points->likes;
         $likes_user = $points->likes_user;
         $ref_user = $points->ref_user;
         $subscription = $points->subscription;
        }
        else{
        $curl = 0;
        $point = "";
        $max = "";
        $min = "";
        $like ="";
        $likes_user ="";
        $ref_user ="";
        $subscription ="";
  } ?>
  <?php $url = (empty($points))? '/inhouse/task_settings/set' : $curl ?>
  <form action="{{ $url }}" method="post">
    <?php $method = ($url === $curl)? 'update' : "" ?>
      <?php if($method == 'update'): ?>
        @Method('PATCH')
     <?php endif ?>
    {{ csrf_field() }}
    <div class="form-group">
      <label for="">How many points make $1?</label>
      <input type="text" class="form-control" name="points" value="{{ ($url == $curl)? $point : '' }}" placeholder="input amount of points equal to a dollar">
    </div>
    <div class="form-group">
      <label for="">Maximum user can withdraw in $?</label>
      <input type="text" class="form-control" name="max" value="{{ ($url == $curl)? $max : '' }}">
    </div>
    <div class="form-group">
      <label for="">Minimum user can withdraw in $?</label>
      <input type="text" class="form-control" name="min" value="{{ ($url == $curl)? $min : '' }}">
    </div>
    <div class="form-group">
      <label for="">How many points make 1 like</label>
      <input type="text" class="form-control" name="likes" value="{{ ($url == $curl)? $like : '' }}">
    </div>
    <div class="form-group">
      <label for="">A Premium user gets how many points for liking another Premium users post?</label>
      <input type="text" class="form-control" name="likes_user" value="{{ ($url == $curl)? $likes_user : '' }}">
    </div>
    <div class="form-group">
      <label for="">User referrals</label>
      <input type="text" class="form-control" name="ref_user" value="{{ ($url == $curl)? $ref_user : '' }}">
    </div>
    <div class="form-group">
      <label for="">Subsscription fee</label>
      <input type="text" class="form-control" name="subscription" value="{{ ($url == $curl)? $subscription : '' }}">
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-primary"  name="" value="{{ ($url === $curl)? 'Update' : 'Set' }}">
    </div>
    @include('error')
  </form>
</div>

@endsection
