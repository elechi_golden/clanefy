@extends('layouts.admin')
@section('content')
<form method="post" action="/inhouse/generate">
  {{ csrf_field() }}
  <div class="form-group col-md-8" style="margin-top:10%">
    <input type="text" class="form-control" name="" value="{{ empty(session('token'))? "" : session('token') }}">
  </div>
  <div class="col-md-12">
    <input type="submit" class="btn btn-primary" value="Generate"/>
  </div>
</form>

@endsection
