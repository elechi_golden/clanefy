@extends('layouts.admin')
@section('content')
<div class="mt-4 col-md-12 table-responsive" style="margin-top:5%">
  <table class="table table-bordered">
    <thead>
      <th>#</th>
      <th>All Users</th>
      <th>Status</th>
    </thead>
    <tbody>
      <?php $i = 1; ?>
      @foreach($users as $user)
        <tr>
          <td>{{ $i }}</td>
          <td>{{ $user->name }}</td>
          <td>
            @if($user->role_id == '5')
                Premium User
              @else
                User
            @endif
          </td>
        </tr>
        <?php $i++; ?>
      @endforeach
    </tbody>
  </table>
</div>

@endsection
