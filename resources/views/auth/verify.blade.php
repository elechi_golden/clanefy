<!DOCTYPE html>
<html>
<head>


  <meta charset="utf-8">
     <meta http-equiv="x-ua-compatible" content="ie=edge">
     <title>Verify | Clanefy</title>
     <meta name="description" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- favicon
 		============================================ -->
     <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png')}}">
     <!-- Google Fonts
 		============================================ -->
     <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
     <!-- Bootstrap CSS
 		============================================ -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link rel="stylesheet" href="{{ asset('css-admin/bootstrap.min.css')}}">
     <!-- Bootstrap CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/font-awesome.min.css')}}">
     <!-- owl.carousel CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/owl.carousel.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/owl.theme.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/owl.transitions.css')}}">
     <!-- animate CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/animate.css')}}">
     <!-- normalize CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/normalize.css')}}">
     <!-- meanmenu icon CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/meanmenu.min.css')}}">
     <!-- main CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/main.css')}}">
     <!-- educate icon CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/educate-custon-icon.css')}}">
     <!-- morrisjs CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/morrisjs/morris.css')}}">
     <!-- mCustomScrollbar CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/scrollbar/jquery.mCustomScrollbar.min.css')}}">
     <!-- metisMenu CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/metisMenu/metisMenu.min.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/metisMenu/metisMenu-vertical.css')}}">
     <!-- calendar CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/calendar/fullcalendar.min.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/calendar/fullcalendar.print.min.css')}}">
     <!-- style CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/style.css')}}">
     <!-- responsive CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/responsive.css')}}">
     <!-- modernizr JS
 		============================================ -->
     <script src="{{ asset('js-admin/vendor/modernizr-2.8.3.min.js')}}"></script>
     @yield('css')
 </head>

 <body>




<div class="error-pagewrap">
		<div class="error-page-int">
			<div class="hpanel">
				<div class="panel-body text-center lock-inner">
					<i class="far fa-check-circle" aria-hidden="true"></i>
					<br/>

					<h4><span class="text-success">Hi</span> <strong>{!! Auth::user()->name !!}</strong></h4>
					<p>{{ __('Please Verify Your Email Address') }}</p>
          <div class="card-body">
              @if (session('resent'))
                  <div class="alert alert-success" role="alert">
                      {{ __('A fresh verification link has been sent to your email address.') }}
                  </div>
              @endif

              {{ __('Before proceeding, please check your email for a verification link.') }}
              {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}" class="btn ">{{ __('click here to request another') }}</a>.
          </div>

				</div>
			</div>
			<div class="text-center login-footer">
				<p>Copyright © 2019. All rights reserved. Clanefy</p>
			</div>
		</div>
	</div>


      <!-- jquery
    		============================================ -->
        <script src="{{ asset('js-admin/vendor/jquery-1.12.4.min.js')}}"></script>
        <!-- bootstrap JS
    		============================================ -->
        <script src="{{ asset('js-admin/bootstrap.min.js')}}"></script>
        <!-- wow JS
    		============================================ -->
        <script src="{{ asset('js-admin/wow.min.js')}}"></script>
        <!-- price-slider JS
    		============================================ -->
        <script src="{{ asset('js-admin/jquery-price-slider.js')}}"></script>
        <!-- meanmenu JS
    		============================================ -->
        <script src="{{ asset('js-admin/jquery.meanmenu.js')}}"></script>
        <!-- owl.carousel JS
    		============================================ -->
        <script src="{{ asset('js-admin/owl.carousel.min.js')}}"></script>
        <!-- sticky JS
    		============================================ -->
        <script src="{{ asset('js-admin/jquery.sticky.js')}}"></script>
        <!-- scrollUp JS
    		============================================ -->
        <script src="{{ asset('js-admin/jquery.scrollUp.min.js')}}"></script>
        <!-- counterup JS
    		============================================ -->
        <script src="{{ asset('js-admin/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{ asset('js-admin/counterup/waypoints.min.js')}}"></script>
        <script src="{{ asset('js-admin/counterup/counterup-active.js')}}"></script>
        <!-- mCustomScrollbar JS
    		============================================ -->
        <script src="{{ asset('js-admin/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <script src="{{ asset('js-admin/scrollbar/mCustomScrollbar-active.js')}}"></script>
        <!-- metisMenu JS
    		============================================ -->
        <script src="{{ asset('js-admin/metisMenu/metisMenu.min.js')}}"></script>
        <script src="{{ asset('js-admin/metisMenu/metisMenu-active.js')}}"></script>
        <!-- morrisjs JS
    		============================================ -->
        <script src="{{asset('js-admin/morrisjs/raphael-min.js')}}"></script>
        <script src="{{ asset('js-admin/morrisjs/morris.js')}}"></script>
        <script src="{{ asset('js-admin/morrisjs/morris-active.js')}}"></script>
        <!-- morrisjs JS
    		============================================ -->
        <script src="{{ asset('js-admin/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{ asset('js-admin/sparkline/jquery.charts-sparkline.js')}}"></script>
        <script src="{{ asset('js-admin/sparkline/sparkline-active.js')}}"></script>
        <!-- calendar JS
    		============================================ -->
        <script src="{{ asset('js-admin/calendar/moment.min.js')}}"></script>
        <script src="{{ asset('js-admin/calendar/fullcalendar.min.js')}}"></script>
        <script src="{{ asset('js-admin/calendar/fullcalendar-active.js')}}"></script>
        <!-- plugins JS
    		============================================ -->
        <script src="{{ asset('js-admin/plugins.js')}}"></script>
        <!-- main JS
    		============================================ -->
        <script src="{{ asset('js-admin/main.js')}}"></script>


      @yield('scripts')
      <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>

      <script>
                          ClassicEditor
                                  .create( document.querySelector( '' ) )
                                  .then( editor => {
                                          console.log( editor );
                                  } )
                                  .catch( error => {
                                          console.error( error );
                                  } );
                  </script>


  </body>
  </html>
