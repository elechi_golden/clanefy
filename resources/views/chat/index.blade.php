@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="column is-8 is-offset-2">
            <div class="panel">
                <div class="panel-heading">
                    List of all Friends

                </div>
                @forelse ($friends as $friends)
                <?php
                $user = App\User::find($friends->requester);
                ?>
                    <a href="{{ route('chat.show', $user->id) }}" class="panel-block" style="justify-content: space-between;">
                        <div>{{$user->name}}</div>
                        <onlineuser v-bind:friend="{{ $friends }}" v-bind:onlineusers="onlineUsers"></onlineuser>
                    </a>
                @empty
                    <div class="panel-block">
                        You don't have any friends
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
