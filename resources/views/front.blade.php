@extends('layouts.front')

@section('intro')
<section id="intro">

   <div class="intro-content">
     <h2> <span></span><br></h2>
     <div>
       @if (Route::has('login'))
           <div class="top-right links">
               @auth
                   <a href="{{ url('/home') }}" class="btn-get-started scrollto">Share Post</a>
               @else
                   <a href="{{ route('login') }}" class="btn-get-started scrollto">Login</a>

                   @if (Route::has('register'))
                       <a href="{{ route('register') }}" class="btn-get-started scrollto">Start Enarning</a>
                   @endif
               @endauth
           </div>
       @endif










     </div>
   </div>

   <div id="intro-carousel" class="owl-carousel" >
     <div class="item" style="background-image: url('img/intro-carousel/1.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/2.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/3.jpg');"></div>
     <div class="item" style="background-image: url('img/intro-carousel/4.jpg');"></div>

   </div>

 </section>

@endsection


@section('main')


<main id="main">

    <!--==========================
      About Section


    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">
        <div class="section-header">
          <h2>Earning as Simple As</h2>

        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><i class="fa fa-users"></i></div>
              <h4 class="title"><a href="">Socializing</a></h4>
              <p class="description">
              Have you ever imagined chatting, posting and liking content, Following trends, and generally being social online
                just like most social media and still earn residual income doing them? That sounds interesting to hear.
                  <a href="" class="btn btn-primary">Read More</a>



              </p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight">
              <div class="icon"><i class="fa fa-tasks"></i></div>
              <h4 class="title"><a href="">Completing Task and Playing Game</a></h4>
              <p class="description">
                you are a user or client that don’t like socializing? You are not left out,
                you also have the tasking option to earn from the website. This is operation your time equals money.

                <a href="" class="btn btn-primary">Read More</a>
              </p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInLeft" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-sign-in"></i></div>
              <h4 class="title"><a href="">Join a Clan</a></h4>
              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur trinige zareta lobur trade.</p>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="box wow fadeInRight" data-wow-delay="0.2s">
              <div class="icon"><i class="fa fa-level-up"></i></div>
              <h4 class="title"><a href="">Multilevel Market Network</a></h4>
              <p class="description">

                The best way to run to the top quickly on our social media,
                 is to get involved in one or more of our various multi-level markets scheme.



              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Clients Section
    ============================-->


    <!--==========================


    <!--==========================
       Section
    ============================-->
    <section id="testimonials" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Recent Activities</h2>

        </div>
        <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item">
              <p>

                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.

              </p>


            </div>

            <div class="testimonial-item">
              <p>

                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>


            </div>

            <div class="testimonial-item">
              <p>

                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.

              </p>

            </div>

            <div class="testimonial-item">
              <p>

                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.

              </p>

            </div>



        </div>

      </div>
    </section><!-- #testimonials -->

	 <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Partners and Third Parties</h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div>

        <div class="owl-carousel clients-carousel">
          <img src="img/clients/client-1.png" alt="">
          <img src="img/clients/client-2.png" alt="">
          <img src="img/clients/client-3.png" alt="">

        </div>

      </div>
    </section><!-- #clients -->

   <!--==========================
      About Section
    ============================-->
    <section id="about" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 about-img">
            <img src="{{ asset('img/mockup.png')}}" alt="">
          </div>

          <div class="col-lg-6 content">
            <div class="section-header">
          <h2>How it Works</h2>

        </div>

            <ul>
              <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="ion-android-checkmark-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul>

          </div>
        </div>

      </div>
    </section><!-- #about -->


  </main>



@endsection
