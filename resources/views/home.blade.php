@extends('layouts.app')

@section('content')

<div class="breadcome-area">
       <div class="container-fluid">
           <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="breadcome-list">
                       <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <div class="breadcome-heading">
                                   <form role="search" class="sr-input-func">
                                       <input type="text" placeholder="Search..." class="search-int form-control">
                                       <a href="#"><i class="fa fa-search"></i></a>
                                   </form>
                               </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <ul class="breadcome-menu">
                                   <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                   </li>

                                   <li><span class="bread-blod">Wall</span>
                                   </li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

<div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                      <div class="product-sales-chart">
                      <div class="container bootstrap snippet">
                        <div class="portlet-title">
                        <div class="row">




        <div class="col-lg-8 col-md-8 col-xs-12">




          <div class="portlet-title">
             <div class="row">
                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                     <div class="caption pro-sl-hd">
                         <span class="caption-subject"><b><h3>News Feed</h3></b></span>
                     </div>
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                     <div class="actions graph-rp graph-rp-dl">
                         <p>Earning on the go</p>
                     </div>
                 </div>
             </div>
             <hr>
         </div>





                                                                @if(count($posts) > 0)
                                                                @foreach ($posts as $post)
                                                                    <div class="chat-discussion" style="height: auto">
                                                                        <div class="chat-message">
                                                                          <?php
                                                                          $user = App\User::where('id', $post->user_id)->first();
                                                                          ?>
                                                                    <div class="profile-hdtc">
                                                                        <img class="message-avatar" src="uploads/avatars/{{ $user->avatar}}" alt="">
                                                                     </div>
                                                                                 <div class="message">
                                                                                        <?php
                                                                                        $user = App\User::where('id', $post->user_id)->first();
                                                                                        ?>
                                                                                     <a class="message-author" href="#">{{$user->name}}</a>
                                                                                     <span class="message-date">{{date('F d, Y', strtotime($post->created_at))}} at {{date('g:ia', strtotime($post->created_at))}}</span>
                                                                                     <span class="message-content">{{$post->post}}</span>
                                                                                     <div class="m-t-md mg-t-10">
                                                                                         <a href="/like/store/{{$post->id}}" class="btn btn-xs btn-default"><i class="fa fa-thumbs-up"></i> Like </a>
                                                                                         <a href="post/{{$post->id}}" class="btn btn-xs btn-success"><i class="far fa-comment"></i> Comment</a>
                                                                                         <a href="post/{{$post->id}}" class="btn btn-xs btn-success"><i class="far fa-"></i> continue reading</a>
                                                                                     </div>
                                                                                 </div>
                                                                            </div>

                                                                        </div>
                                                                @endforeach
                                                                         {{$posts->links()}}
                                                                @else
                                                                                     <p>No posts found</p>
                                                                @endif






</div>




</div>
</div>

</div>



                    </div>
                  </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <div class="single-cards-item">
                          <div class="single-product-image">
                              <a href="#"><img src="img/bg-01.jpg" alt=""></a>
                          </div>
                          <div class="single-product-text">
                              <img src="img/img.jpg" alt="">
                              <h4><a class="cards-hd-dn" href="#">Elechi GOlden</a></h4>
                              <h5>Web Designer & Developer</h5>
                              <p class="ctn-cards">Lorem ipsum dolor sit amet, this is a consectetur adipisicing elit</p>
                              <a class="follow-cards" href="#">Follow</a>
                              <div class="row">
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <div class="cards-dtn">
                                          <h3><span class="counter">199</span></h3>
                                          <p>Articles</p>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <div class="cards-dtn">
                                          <h3><span class="counter">599</span></h3>
                                          <p>Like</p>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      <div class="cards-dtn">
                                          <h3><span class="counter">399</span></h3>
                                          <p>Comment</p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>



                    </div>
                </div>
            </div>
        </div>


        <div class="library-book-area mg-t-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                      <div class="white-box analytics-info-cs mg-b-10 res-mg-b-30 tb-sm-res-d-n dk-res-t-d-n">
                          <h3 class="box-title">Total View</h3>
                          <ul class="list-inline two-part-sp">
                              <li>
                                  <div id="sparklinedash2"></div>
                              </li>
                              <li class="text-right graph-two-ctn"><i class="fa fa-level-up" aria-hidden="true"></i> <span class="counter text-purple">3000</span></li>
                          </ul>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                            <div class="single-review-st-hd">
                                <h2>People you May Connect with</h2>
                            </div>
                            <div class="single-review-st-text">
                                <img src="img/img.jpg" alt="">
                                <div class="review-ctn-hf">
                                    <h3>Sarah Graves</h3>
                                    <p>Member: VIP</p>
                                </div>

                                    <div class="product-buttons review-item-rating">
                                  <button type="button" class="button-default cart-btn">Connect</button>
                                </div>
                            </div>
                            <div class="single-review-st-text">
                                <img src="img/img.jpg" alt="">
                                <div class="review-ctn-hf">
                                    <h3>Garbease sha</h3>
                                    <p>Awesome Pro</p>
                                </div>
                                <div class="review-item-rating">
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star-half"></i>
                                </div>
                            </div>
                            <div class="single-review-st-text">
                                <img src="img/img.jpg" alt="">
                                <div class="review-ctn-hf">
                                    <h3>Gobetro pro</h3>
                                    <p>Great Website</p>
                                </div>
                                <div class="review-item-rating">
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star-half"></i>
                                </div>
                            </div>
                            <div class="single-review-st-text">
                                <img src="img/img.jpg" alt="">
                                <div class="review-ctn-hf">
                                    <h3>Siam Graves</h3>
                                    <p>That's Good</p>
                                </div>
                                <div class="review-item-rating">
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star-half"></i>
                                </div>
                            </div>
                            <div class="single-review-st-text">
                                <img src="img/img.jpg" alt="">
                                <div class="review-ctn-hf">
                                    <h3>Sarah Graves</h3>
                                    <p>Highly recommend</p>
                                </div>
                                <div class="review-item-rating">
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star-half"></i>
                                </div>
                            </div>
                            <div class="single-review-st-text">
                                <img src="img/img.jpg" alt="">
                                <div class="review-ctn-hf">
                                    <h3>Julsha Grav</h3>
                                    <p>Sei Hoise bro</p>
                                </div>
                                <div class="review-item-rating">
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star"></i>
                                    <i class="educate-icon educate-star-half"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-product-item res-mg-t-30 table-mg-t-pro-n tb-sm-res-d-n dk-res-t-d-n">
                          <div class="single-review-st-hd">
                              <h2>Sponsored Post</h2>
                          </div>
                            <div class="single-product-image">
                                <a href="#"><img src="img/product/book-4.jpg" alt=""></a>
                            </div>
                            <div class="single-product-text edu-pro-tx">
                                <h4><a href="#">Title Demo Here</a></h4>
                                <h5>Lorem ipsum dolor sit amet, this is a consec tetur adipisicing elit</h5>
                                <div class="product-price">
                                    <h3>$45</h3>
                                    <div class="single-item-rating">
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star"></i>
                                        <i class="educate-icon educate-star-half"></i>
                                    </div>
                                </div>
                                <div class="product-buttons">
                                    <button type="button" class="button-default cart-btn">Read More</button>
                                    <button type="button" class="button-default"><i class="fa fa-heart"></i></button>
                                    <button type="button" class="button-default"><i class="fa fa-share"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection
