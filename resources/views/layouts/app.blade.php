 <!DOCTYPE html>
<html>
<head>


  <meta charset="utf-8">
     <meta http-equiv="x-ua-compatible" content="ie=edge">
     <title>Home | Clanefy</title>
     <meta name="description" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="userId" content="{{ Auth::check() ? Auth::user()->id : 'null'}}"
     <!-- favicon
 		============================================ -->
     <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/fav.png')}}">
     <!-- Google Fonts
 		============================================ -->
     <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
     <!-- Bootstrap CSS
 		============================================ -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link rel="stylesheet" href="{{ asset('css-admin/bootstrap.min.css')}}">

     <!-- Bootstrap CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/font-awesome.min.css')}}">
     <!-- owl.carousel CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/owl.carousel.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/owl.theme.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/owl.transitions.css')}}">
     <!-- animate CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/animate.css')}}">
     <!-- normalize CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/normalize.css')}}">
     <!-- meanmenu icon CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/meanmenu.min.css')}}">
     <!-- main CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/main.css')}}">
     <!-- educate icon CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/educate-custon-icon.css')}}">
     <!-- morrisjs CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/morrisjs/morris.css')}}">
     <!-- mCustomScrollbar CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/scrollbar/jquery.mCustomScrollbar.min.css')}}">
     <!-- metisMenu CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/metisMenu/metisMenu.min.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/metisMenu/metisMenu-vertical.css')}}">
     <!-- calendar CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/calendar/fullcalendar.min.css')}}">
     <link rel="stylesheet" href="{{ asset('css-admin/calendar/fullcalendar.print.min.css')}}">
     <!-- modals CSS
   ============================================ -->
   <link rel="stylesheet" href="{{ asset('css-admin/modals.css')}}">
     <!-- style CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/style.css')}}">
     <!-- responsive CSS
 		============================================ -->
     <link rel="stylesheet" href="{{ asset('css-admin/responsive.css')}}">
     <!-- modernizr JS
 		============================================ -->
     <script src="{{ asset('js-admin/vendor/modernizr-2.8.3.min.js')}}"></script>
     @yield('css')
 </head>

 <body>
     <!--[if lt IE 8]>
 		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 	<![endif]-->

  @if (!Auth::guest())
     <!-- Start Left menu area -->
     <div class="left-sidebar-pro">
         <nav id="sidebar" class="">
             <div class="sidebar-header">
                 <a href="index.html"><img class="main-logo" src="img/log.png" alt="" /></a>
                 <strong><a href="index.html"><img src="img/log.png" alt="" /></a></strong>
             </div>
             <div class="left-custom-menu-adp-wrap comment-scrollbar">
                 <nav class="sidebar-nav left-sidebar-menu-pro">
                     <ul class="metismenu" id="menu1">
                         <li class="">
                             <a class="" href="index.html" aria-expanded="false"><span class="mini-sub-pro">
 								                      Dashboard
 								                    </a>

                         </li>

                         <li>
                             <a class="has-arrow" href="" aria-expanded="false"><span class="educate-icon educate-professor icon-wrap"></span> <span class="mini-click-non">Clan</span></a>
                             <ul class="submenu-angle" aria-expanded="false">
                                 <li><a title="All Professors" href=""><span class="mini-sub-pro">My Clane</span></a></li>
                                 <li><a title="Add Professor" href="add-professor.html"><span class="mini-sub-pro">Global Chat</span></a></li>

                             </ul>
                         </li>
                         <li>
                             <a class="has-arrow" href="" aria-expanded="false"><span class="educate-icon educate-student icon-wrap"></span> <span class="mini-click-non">Earn</span></a>
                             <ul class="submenu-angle" aria-expanded="false">
                               <li><a title="Refer 'N' Earn" href=""><span class="mini-sub-pro">Refer 'N' Earn</span></a></li>
                               <li><a title="Tasks" href=""><span class="mini-sub-pro">Complete Task</span></a></li>

                             </ul>
                         </li>
                         <li>
                             <a title="Landing Page" href="/tickets" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Support</span></a>
                         </li>
                         <li>
                             <a title="Landing Page" href="{!! route('refCategories.index') !!}" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Ref Categories</span></a>
                         </li>


                     </ul>
                 </nav>
             </div>
         </nav>
     </div>
     <!-- End Left menu area -->
     <!-- Start Welcome area -->

     <div class="all-content-wrapper">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="logo-pro">
                         <a href="index.html"><img class="main-logo" src="img/log.png" alt="" /></a>
                     </div>
                 </div>
             </div>
         </div>

         @include('layouts.notification')

             <!-- Mobile menu -->




        </div>


        <div class="login-form-area edu-pd mg-b-15">
            <div class="container-fluid">

        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="tab-content-details shadow-reset">
                                    <h2>Hi, {!! Auth::user()->name !!}</h2>
                                    <p>What do you have to share today</p>

                                          <a href="" class="btn btn-success widget-btn-1 btn-sm review-item-rating" data-toggle="modal" data-target="#Post"><b>Publish</b></a>
                                </div>


                            </div>
                        </div>


            </div>
          </div>










        <div class="content-wrapper">
            @if (session('status'))
            <div class="alert alert-info">
                {{ session('status') }}
            </div>
        @endif
            @yield('content')
        </div>



         <div class="footer-copyright-area">
             <div class="container-fluid">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="footer-copy-right">
                             <p>Copyright © 2019. All rights reserved. Clanefy</p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>


             @else
                 <nav class="navbar navbar-default navbar-static-top">
                     <div class="container">
                         <div class="navbar-header">

                             <!-- Collapsed Hamburger -->
                             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                     data-target="#app-navbar-collapse">
                                 <span class="sr-only">Toggle Navigation</span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                                 <span class="icon-bar"></span>
                             </button>

                             <!-- Branding Image -->
                             <a class="navbar-brand" href="{!! url('/') !!}">

                             </a>
                         </div>

                         <div class="collapse navbar-collapse" id="app-navbar-collapse">
                             <!-- Left Side Of Navbar -->
                             <ul class="nav navbar-nav">
                                 <li><a href="{!! url('/home') !!}">Home</a></li>
                             </ul>

                             <!-- Right Side Of Navbar -->
                             <ul class="nav navbar-nav navbar-right">
                                 <!-- Authentication Links -->
                                 <li><a href="{!! url('/login') !!}">Login</a></li>
                                 <li><a href="{!! url('/register') !!}">Register</a></li>
                             </ul>
                         </div>
                     </div>
                 </nav>




    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                </div>
            </div>
        </div>
    </div>
    @endif
</div>





<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

<div id="Post" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
<div class="modal-dialog">
    <div class="modal-content">

         <form method="POST" action="post/store" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <textarea class="form-control" name="post" cols="30" rows="10" ></textarea>
            </div>

            <div class="form-group">
                <div class="modal-footer text-center">
                    <input type="file" name="picture" id="picture" class="far fa-images">
                    <button type="submit"  name="submit">Submit</button>
                        <a data-dismiss="modal" href="#">Cancel</a>
                </div>
            </div>
         </form>
    </div>
</div>
</div>





</div>

</div>








    <!-- jquery
  		============================================ -->
      <script src="{{ asset('js-admin/vendor/jquery-1.12.4.min.js')}}"></script>
      <!-- bootstrap JS
  		============================================ -->
      <script src="{{ asset('js-admin/bootstrap.min.js')}}"></script>
      <!-- wow JS
  		============================================ -->
      <script src="{{ asset('js-admin/wow.min.js')}}"></script>
      <!-- price-slider JS
  		============================================ -->
      <script src="{{ asset('js-admin/jquery-price-slider.js')}}"></script>
      <!-- meanmenu JS
  		============================================ -->
      <script src="{{ asset('js-admin/jquery.meanmenu.js')}}"></script>
      <!-- owl.carousel JS
  		============================================ -->
      <script src="{{ asset('js-admin/owl.carousel.min.js')}}"></script>
      <!-- sticky JS
  		============================================ -->
      <script src="{{ asset('js-admin/jquery.sticky.js')}}"></script>
      <!-- scrollUp JS
  		============================================ -->
      <script src="{{ asset('js-admin/jquery.scrollUp.min.js')}}"></script>
      <!-- counterup JS
  		============================================ -->
      <script src="{{ asset('js-admin/counterup/jquery.counterup.min.js')}}"></script>
      <script src="{{ asset('js-admin/counterup/waypoints.min.js')}}"></script>
      <script src="{{ asset('js-admin/counterup/counterup-active.js')}}"></script>
      <!-- mCustomScrollbar JS
  		============================================ -->
      <script src="{{ asset('js-admin/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
      <script src="{{ asset('js-admin/scrollbar/mCustomScrollbar-active.js')}}"></script>
      <!-- metisMenu JS
  		============================================ -->
      <script src="{{ asset('js-admin/metisMenu/metisMenu.min.js')}}"></script>
      <script src="{{ asset('js-admin/metisMenu/metisMenu-active.js')}}"></script>
      <!-- morrisjs JS
  		============================================ -->
      <script src="{{asset('js-admin/morrisjs/raphael-min.js')}}"></script>
      <script src="{{ asset('js-admin/morrisjs/morris.js')}}"></script>
      <script src="{{ asset('js-admin/morrisjs/morris-active.js')}}"></script>
      <!-- morrisjs JS
  		============================================ -->
      <script src="{{ asset('js-admin/sparkline/jquery.sparkline.min.js')}}"></script>
      <script src="{{ asset('js-admin/sparkline/jquery.charts-sparkline.js')}}"></script>
      <script src="{{ asset('js-admin/sparkline/sparkline-active.js')}}"></script>
      <!-- calendar JS
  		============================================ -->
      <script src="{{ asset('js-admin/calendar/moment.min.js')}}"></script>
      <script src="{{ asset('js-admin/calendar/fullcalendar.min.js')}}"></script>
      <script src="{{ asset('js-admin/calendar/fullcalendar-active.js')}}"></script>
      <!-- plugins JS
  		============================================ -->
      <script src="{{ asset('js-admin/plugins.js')}}"></script>
      <!-- main JS
  		============================================ -->
      <script src="{{ asset('js-admin/main.js')}}"></script>


    @yield('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>

    <script>
                        ClassicEditor
                                .create( document.querySelector( '' ) )
                                .then( editor => {
                                        console.log( editor );
                                } )
                                .catch( error => {
                                        console.error( error );
                                } );
                </script>


</body>
</html>
