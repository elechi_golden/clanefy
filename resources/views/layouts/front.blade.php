<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Clanefy: Share Earn</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/fav.png')}}">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{ asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{ asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{ asset('lib/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
  <link href="{{ asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('css/style.css')}}" rel="stylesheet">


</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:contact@example.com"></a>
        <i class="fa fa-phone"></i>
      </div>

    </div>
  </section>


<header id="header">
 <div class="container">

   <div id="logo" class="pull-left">

     <!-- Uncomment below if you prefer to use an image logo -->
      <a href="#body"><img src="{{ asset('img/new.png')}}" alt="" title="" /></a>
   </div>

   <nav id="nav-menu-container">
 <ul class="nav-menu1">



      @if (Route::has('login'))

              @auth
                  <a href="{{ url('/home') }}" class="btn btn-info btn1">Timeline</a>
              @else
                  <a href="{{ route('login') }}" class="btn-get-started scrollto">Login</a>

                  @if (Route::has('register'))
                      <a href="{{ route('register') }}" class="btn btn-info btn1">Start Earning</a>
                  @endif
              @endauth

      @endif




 </ul>

     <ul class="nav-menu">
 <hr>
       <li class="menu-active hide"><a href="#body">Home</a></li>


       <li><a href="">Trending</a></li>
       <li><a href="">Payment</a></li>
       <li><a href="">Partner</a></li>
   <li><a href="">Support</a></li>
       <li><a href="">Video of Happy Users</a></li>


     </ul>


   </nav><!-- #nav-menu-container -->
 </div>
<ul class="nav-men">


</ul>
</header><!-- #header -->




<!--==========================
    Intro Section
  ============================-->
  <section id="intro">

    @yield('intro')

  </section>


<main id="main">

  @yield('main')

</main>





<footer id="footer">
    <div class="container">
	<div class="row fot text-center d-flex justify-content-center pt-5 mb-3">

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="#!">About us</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-2">
          <h6 class="text-uppercase font-weight-bold">
            <a href="#!">Pricing</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-2">
          <h6 class="text-uppercase font-weight-bold">
            <a href="#!">How it works</a>
          </h6>
        </div>
        <!-- Grid column -->
		<!-- Grid column -->
        <div class="col-md-2 mb-2">
          <h6 class="text-uppercase font-weight-bold">
            <a href="#!">Terms and Conditions</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="#!">Disclaimer</a>
          </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
          <h6 class="text-uppercase font-weight-bold">
            <a href="#!">Support</a>
          </h6>
        </div>
        <!-- Grid column -->

      </div>
	   <hr class="rgba-white-light" style="margin: 0 15%;">


      <div class="copyright">
        &copy; 2019 Copyright <strong>Clanefy</strong>. All Rights Reserved
      </div>
	  <!-- Grid row-->
      <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

        <!-- Grid column -->
        <div class="col-md-8 col-12 mt-5">
          <p style="line-height: 1.7rem">In Clanefy You are paid for ever Step.</p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
  </footer><!-- #footer -->










<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('lib/easing/easing.min.js')}}"></script>
  <script src="{{ asset('lib/superfish/hoverIntent.js')}}"></script>
  <script src="{{ asset('lib/superfish/superfish.min.js')}}"></script>
  <script src="{{ asset('lib/wow/wow.min.js')}}"></script>
  <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('lib/magnific-popup/magnific-popup.min.js')}}"></script>
  <script src="{{ asset('lib/sticky/sticky.js')}}"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('js/main.js')}}"></script>



</body>
</html>
