<?php
use App\Models\PostsNotification;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Friendships;

$notifications = PostsNotification::where('user_notified', Auth::id())->where('status','unread')->get();

$requests = Friendships::where('user_requested', '=' , Auth::id())
->where('status', '=', 'pending')
->get();

$numRequests = Count($requests);
$numNotifications = Count($notifications);
$userRole = Auth::User()->getRole(Auth::id());
?>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                         <i class="fas fa-ellipsis-h"></i>
                       </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                                        <div class="header-top-menu tabl-d-n">
                                            <ul class="nav navbar-nav mai-top-nav">
                                                <li class="nav-item"><a href="/home" class="nav-link">Home</a>
                                                </li>
                                                <li class="nav-item"><a href="/about" class="nav-link">About</a>
                                                </li>
                                                <li class="nav-item"><a href="#" class="nav-link">Investment</a>
                                                </li>

                                                <li class="nav-item"><a href="/tickets" class="nav-link">Support</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                                <li class="nav-item dropdown">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="far fa-envelope" aria-hidden="true"></i><span class="indicator-ms"></span></a>
                                                    <div role="menu" class="author-message-top dropdown-menu animated zoomIn">
                                                        <div class="message-single-top">
                                                            <h1>Message</h1>
                                                        </div>
                                                        <ul class="message-menu">
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/1.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Advanda Cro</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/4.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Sulaiman din</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>


                                                        </ul>
                                                        <div class="message-view">
                                                            <a href="#">View All Messages</a>
                                                        </div>
                                                    </div>
                                                </li>



                                                <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fas fa-users" aria-hidden="true">
                                                </i>
                                                @if($numRequests != 0 )
                                                <span class="indicator-nt "></span>

                                                @endif

                                              </a>
                                                    <div role="menu" class="notification-author dropdown-menu animated zoomIn">
                                                        <div class="notification-single-top">

                                                            <h1>Connection Request ({{$numRequests}})</h1>


                                                        </div>
                                                        <ul class="notification-menu">
                                                        @foreach ($requests as $request)
                                                            <li>
                                                                <a href="/connect/requests">
                                                                  <?php
                                                                  $sender =  App\User::where('id', $request->requester)->first();
                                                                  ?>
                                                                    <div class="notification-icon">
                                                                      <img alt="logo" class="educate-icon img-circle m-b" height="50px" width="50px" src="{{ asset ('uploads/avatars')}}/{{$sender->avatar}}">

                                                                    </div>

                                                                    <div class="notification-content">
                                                                        <span class="notification-date">{{date('F d, Y', strtotime($request->created_at))}} at {{date('g:ia', strtotime($request->created_at))}}</span>
                                                                        <br>
                                                                        <h2>{{$sender->name}}</h2>
                                                                        <p>Wants to connect with You.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                        </ul>
                                                        <div class="notification-view">
                                                            <a href="#">View All Connections</a>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="far fa-bell" aria-hidden="true"></i>
                                                  @if ($numNotifications != 0)
                                                  <span class="indicator-nt"></span>
                                                  @endif
                                                </a>
                                                    <div role="menu" class="notification-author dropdown-menu animated zoomIn">
                                                        <div class="notification-single-top">
                                                            <h1>Notifications ({{$numNotifications}})</h1>
                                                        </div>

                                                        <ul class="notification-menu">
                                                            @foreach ($notifications as $notification)

                                                            <li>
                                                                <a href="notification/{{$notification->id}}">
                                                                    <div class="notification-icon">
                                                                        <i class="educate-icon educate-checked edu-checked-pro admin-check-pro" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">{{date('F d, Y', strtotime($notification->created_at))}} at {{date('g:ia', strtotime($notification->created_at))}}</span>
                                                                       <br>
                                                                        <h2></h2>
                                                                        <p>{{$notification->notification}}</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            @endforeach

                                                        </ul>
                                                        <div class="notification-view">
                                                            <a href="#">View All Notification</a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                             <img src="{{ asset ('uploads/avatars')}}/{{ Auth::user()->avatar }}" alt="avatar" />
                             <span class="admin-name">{!! Auth::user()->name !!}</span>
                             <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                           </a>
                                                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                        <li><a href="/myprofile"><span class="edu-icon edu-home-admin author-log-ic"></span>My Profile</a>
                                                        </li>
                                                        <li><a href="/connect"><span class="edu-icon edu-user-rounded author-log-ic"></span>Connections</a>
                                                        </li>
                                                        @if (Auth::user()->role_id !=5)
                                                        <li><a href="#" data-toggle="modal" data-target="#PrimaryModalhdbgcl"><span class="edu-icon edu-user-rounded author-log-ic"></span>Upgrade to Premium</a>
                                                        </li>


                                                        @else(Auth::user()->role_id !=5)
                                                        <li><a href="/wallet" ><span class="edu-icon edu-money author-log-ic"></span>My Wallet</a>
                                                        </li>
                                                        @endif



                                                        <li><a href="/taskUsers"><span class="edu-icon edu-settings author-log-ic"></span>My Tasks</a>
                                                        </li>
                                                      </li>

                                                      <li><a href="/connect/requests"><span class="edu-icon edu-settings author-log-ic"></span>Connection Request</a>
                                                      </li>


                                                        <li><a href="{!! url('/logout') !!}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" ><span class="edu-icon edu-locked author-log-ic"></span>Log Out</a>


                                                          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                        </form>
                                                        </li>

                                                    </ul>
                                                </li>
                                                <li class="nav-item nav-setting-open"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="far fa-compass"></i></a>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                             <div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                                 <div class="modal-dialog">
                                     <div class="modal-content">
                                         <div class="modal-header header-color-modal bg-color-1">
                                             <h4 class="modal-title">Enter Your Upgrade Code</h4>
                                             <div class="modal-close-area modal-close-df">
                                                 <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                             </div>
                                         </div>



                                         <form action="/home/upgrade" method="post">
                                           {{ csrf_field() }}
                                           <div class="modal-body">

                                             <div class="form-group">
                                               <input name="code" type="text" class="form-control" placeholder="Enter Code">
                                            </div>
                                           <div class="modal-footer">
                                               <a data-dismiss="modal" class="btn btn-primary btn-lg" href="#">Cancel</a>
                                               <input type="submit" class="btn btn-primary" value="Upgrade">
                                           </div>
                                       </div>

                                   </form>

                                 </div>
                             </div>



                         </div>
                       </div>
                       <div class="login-form-area edu-pd mg-b-15">
                           <div class="container-fluid">

                       <div class="row">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                                                           @if(!empty(session('success')))
                                                            <div class="alert alert-success">
                                                              <p>{{ session('success') }}</p>
                                                            </div>
                                                          @endif
                                                           @if(!empty(session('error')))
                                                           <div class="alert alert-danger">
                                                            @foreach(session('error') as $error)
                                                                <p>{{ $error }}</p>
                                                            @endforeach
                                                          </div>
                                                          @endif
                                                           @if($errors->any())
                                                            @foreach($errors->all() as $error)
                                                              <div class="alert alert-danger">
                                                                <p>{{ $error }}</p>
                                                              </div>
                                                            @endforeach
                                                          @endif

                        </div>
                      </div>
                      </div>
