<table class="table table-responsive" id="tasks-table">
    <thead>
        <tr>
            <th></th>

        <th>Point(s)</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
      <?php $count = 1 ?>
    @foreach($tasks as $task)

        <tr>
            <td>
              <h2>{{$count++ }}.<a href="{!! route('tasks.show', [$task->id]) !!}"> {!! $task->title !!}</a></h2>
              <h3>Instruction</h3>
              <p>{!! $task->instruction !!}</p>

            </td>

            <td><h3>{!! $task->reward_points !!}</h3></td>
            <td>

                <div class='btn-group'>
                  @if(Auth::user()->role_id < 3)
                  {!! Form::open(['route' => ['tasks.destroy', $task->id], 'method' => 'delete']) !!}



                    {!! Form::close() !!}
                  @endif
                </div>

            </td>
        </tr>
        <tr>
          <?php $taskCompleted = 0;  ?>
          <td>
            @foreach($taskUsers as $taskUser)

              @if($taskUser->task_id == $task->id)
            <?php $taskCompleted = 1; ?>
              @endif

            @endforeach

            @if($taskCompleted ==0)

            {!! Form::open(['route' => 'taskUsers.store']) !!}
              @include('task_users.fields')

              {!! Form::close() !!}
              @else
              <div class="text-center text-red" >

             @if($taskUser->status == 1)
             <div class="alert alert-success " role="alert">
                   Task Completed And Approved
            </div>
                 @elseif($taskCompleted == 1 && $taskUser->status == 0)

                   <div class="alert alert-danger " role="alert">
                     You have submitted this task for approval
                  </div>
                 @endif
            </div>
            @endif



          </td>
          <td>
            @if($taskUser->status == 1 && $taskCompleted == 1)
              <p class="text-green">    Approved </p>
            @elseif($taskCompleted == 1 && $taskUser->status == 0)
              <p class="text-red">    Pending approval </p>
            @endif
          </td>
          <td></td>
        </tr>
    @endforeach
    </tbody>
</table>
