
@extends('layouts.app')

@section('content')
<div class="blog-details-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="blog-details-inner">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="latest-blog-single blog-single-full-view">

                                      <div class="table-responsive ib-tb">
                                    <div class="table table-hover table-mailbox">


                                      <ul>
                                        <tr class="unread">

                                                <span>Moses Golden</span><span class="label label-info">Sponsored</span>
                                                <a href="/post/{id}">
                                                <td>this is where all the content goes</td>

                                                <i class="fa fa-paperclip"></i>
                                                <span class="text-right mail-date">Tue, Nov 25</span>
                                              </a>
                                            </tr>
                                          </ul>

                                          <hr>
                                            <ul>
                                            <tr class="unread">
                                                    <span>Jeremy Massey</span>
                                                    <a href="/post/{id}">
                                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>

                                                    <i class="fa fa-paperclip"></i>
                                                    <span class="text-right mail-date">Tue, Nov 25</span>
                                                  </a>
                                                </tr>

                                              </ul>

                                    </div>
                                </div>

                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
