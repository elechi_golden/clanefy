
@extends('layouts.app')
<?php
$comments = App\Models\Post::find($posts->id)->comments;
$likes = App\Models\Post::find($posts->id)->likes;
?>

@section('content')
<div class="blog-details-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="blog-details-inner">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="latest-blog-single blog-single-full-view">
                                        <div class="blog-image">
                                            <a href="#"><img src="img/blog-details/1.jpg" alt="" />
												</a>
                                            <div class="blog-date">
                                                <p><span class="blog-day">{{date('d', strtotime($posts->created_at))}}</span> {{substr(date('F', strtotime($posts->created_at)), 0, 3)}}</p>
                                            </div>
                                        </div>
                                        <div class="blog-details blog-sig-details">
                                            <div class="details-blog-dt blog-sig-details-dt courses-info mobile-sm-d-n">
                                                <span><i class="fas fa-eye"></i> 23</span>

                                                <span><i class="fa fa-thumbs-up"></i> {{count($likes)}} </a></spam>
                                                <span><i class="far fa-comment blue"></i> {{count($comments)}}</a></spam>
                                            </div>

                                            <p>{{$posts->post}}</p>
                                        </div>
                                        <div class="m-t-md mg-t-10">
                                            <a class="btn btn-xs btn-default" href="/like/store/{{$posts->id}}"><i class="fa fa-thumbs-up"></i> Cheer </a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="comment-head">
                                        <h3>Comments</h3>
                                    </div>
                                </div>
                            </div>
                            @foreach ($comments as $comment)
                                @if (!empty($comment))
                                <?php
                                $commentUser = App\User::where('id', $comment->user_id)->first();
                                ?>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="user-comment">
                                                <img src="img/contact/1.jpg" alt="" />
                                                <div class="comment-details">
                                                    <h4>{{$commentUser->name}} | {{date('F d, Y', strtotime($comment->created_at))}} at {{date('g:ia', strtotime($comment->created_at))}} <span class="comment-replay">Replay</span></h4>
                                                    <p>{{$comment->comment}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="comment-details">
                                        <h4>No Comment Found</h4>
                                    </div>
                            @endif                                
                            @endforeach

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="lead-head">
                                        <h3>Leave A Comment</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="coment-area">
                                    <form method="POST" action="/comment/store" class="comment">
                                        @csrf
                                        <input type="hidden" name="post_id" value="{{$posts->id}}">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <textarea name="comment" cols="30" rows="10" placeholder="comment"></textarea>
                                            </div>
                                            <div class="payment-adress comment-stn">
                                                <button name="submit" type="submit" class="btn btn-primary waves-effect waves-light">Comment</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
