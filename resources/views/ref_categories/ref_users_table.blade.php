<table class="table table-responsive" id="refCategories-table">
  <?php if($ref_users != null): ?>
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
            <?php $i = 1; ?>
            <?php foreach ($ref_users as $key => $value): ?>
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $value}}</td>
                <td>{{ $key }}</td>
            </tr>
            <?php $i++ ?>
            <?php endforeach; ?>
    </tbody>
  <?php endif ?>
</table>
