<table class="table table-responsive" id="refCategories-table">
  <?php if($refCategories != null): ?>
    <thead>
        <tr>
            <th>Name</th>
            <th>Ref url</th>
            <th>Referred by</th>
            <th>Visits</th>
              <th>SignUps</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
          <tr>
              <td>{!! $refCategories->name !!}</td>
              <td>{!! route('refs.user',['user_id'=>$refCategories->user_id,'ref_category_id'=>$refCategories->id]) !!}</td>
              <td>{{ $referred_by['0'] }}</td>
              <td>{!! $refCategories->ref_visit !!}</td>
              <td>{!! $refCategories->ref_count !!}</td>
              <td>
                  {!! Form::open(['route' => ['refCategories.destroy', $refCategories->id], 'method' => 'delete']) !!}
                  <div class='btn-group'>
                      <a href="{!! route('refCategories.show', [$refCategories->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                      <a href="{!! route('refCategories.edit', [$refCategories->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                      {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                  </div>
                  {!! Form::close() !!}
              </td>
          </tr>
    </tbody>
  <?php else: ?>
    <p class="alert alert-warning">No Referral links added by you yet</p>
  <?php endif ?>
</table>
