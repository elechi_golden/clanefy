@extends('layouts.admin')

@section('content')
<div class="widget-program-box mg-tb-30">
            <div class="container-fluid">
    <section class="content-header">
        <h1 class="pull-left">Reflevels</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('reflevels.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('reflevels.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
  </div>
@endsection
