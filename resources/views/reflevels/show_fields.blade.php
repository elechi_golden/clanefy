<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $reflevel->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $reflevel->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $reflevel->description !!}</p>
</div>

<!-- Reward Field -->
<div class="form-group">
    {!! Form::label('reward', 'Reward:') !!}
    <p>{!! $reflevel->reward !!}</p>
</div>

<!-- Congratulatory Message Field -->
<div class="form-group">
    {!! Form::label('congratulatory_message', 'Congratulatory Message:') !!}
    <p>{!! $reflevel->congratulatory_message !!}</p>
</div>

<!-- Target No Referrals Field -->
<div class="form-group">
    {!! Form::label('target_no_referrals', 'Target No Referrals:') !!}
    <p>{!! $reflevel->target_no_referrals !!}</p>
</div>

<!-- Point Per Referral Field -->
<div class="form-group">
    {!! Form::label('point_per_referral', 'Point Per Referral:') !!}
    <p>{!! $reflevel->point_per_referral !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $reflevel->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $reflevel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $reflevel->updated_at !!}</p>
</div>

