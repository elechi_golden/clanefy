<table class="table table-responsive" id="reflevels-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Description</th>
        <th>Reward</th>
        <th>Congratulatory Message</th>
        <th>Target No Referrals</th>
        <th>Point Per Referral</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reflevels as $reflevel)
        <tr>
            <td>{!! $reflevel->name !!}</td>
            <td>{!! $reflevel->description !!}</td>
            <td>{!! $reflevel->reward !!}</td>
            <td>{!! $reflevel->congratulatory_message !!}</td>
            <td>{!! $reflevel->target_no_referrals !!}</td>
            <td>{!! $reflevel->point_per_referral !!}</td>
            <td>
                {!! Form::open(['route' => ['reflevels.destroy', $reflevel->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('reflevels.show', [$reflevel->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('reflevels.edit', [$reflevel->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
