@if(!empty(session('success')))
  <div class="alert alert-success" style="width:inherit">
    <p>{{ session('success') }}</p>
  </div>
@endif
