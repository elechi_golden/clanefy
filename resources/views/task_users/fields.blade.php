<!-- User Id Field -->


<!-- Task Id Field -->

    {!! Form::hidden('task_id', $task->id, ['class' => 'form-control']) !!}


<!-- Reward Points Field -->


    {!! Form::hidden('reward_points', $task->reward_points, ['class' => 'form-control']) !!}


<!-- Proof Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('proof', 'Proof:') !!}
    {!! Form::textarea('proof', null, ['class' => 'form-control', 'rows'=> 2]) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

</div>
