@extends('layouts.app')

@section('content')
    <section class="content-header">



    </section>
    <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix">
          <spam class="text-right">
            <?php $totalPointEarned = 0;
                  $totalPointPending = 0;
             ?>
            @foreach($taskUsers as $taskUser)
              @if($taskUser->status == 1)
                <?php
                  $totalPointEarned = $taskUser->reward_points + $totalPointEarned;

                 ?>
              @endif

              @if($taskUser->status == 0)
              <?php
                $totalPointPending = $taskUser->reward_points + $totalPointPending;

               ?>

              @endif

            @endforeach
              Pending Points: {{$totalPointPending}}  | Points Earned: {{$totalPointEarned}}
          </spam>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('task_users.table')
            </div>
        </div>
        <div class="text-center">

        </div>
        <div>
        </div>
    </div>
  </div>
</div>
@endsection
