<table class="table table-responsive" id="taskUsers-table">
    <thead>
        <tr>
            <th>User</th>


        <th>Proof</th>
        <th>Reward Points</th>
        <th>Status</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($taskUsers as $taskUser)
        <tr>
            <td><a href="/users/{!! $taskUser->user_id !!}& {{ $taskUser->user['name']}} ">
              {{ $taskUser->user['email']}}</a></td>
            <td>{!! $taskUser->proof !!}</td>
            <td>{!! $taskUser->reward_points !!}</td>
            <td>
              @if($taskUser->status== 0)
                <spn><i class="glyphicon glyphicon-ok"></i>Pending</span>
                @elseif($taskUser->status== 1)
              <spn><i class="glyphicon glyphicon-ok"></i>  Approved </span>
                @else
                Denied
                @endif


            </td>
            <td>

                <div class='btn-group'>
                  {!! Form::open(['route' => ['taskUsers.change', $taskUser->id, 1], 'method' => 'post']) !!}
                  {!! Form::button('<i class="glyphicon glyphicon-ok"></i>Approve', ['type' => 'submit', 'class' => 'btn btn-success btn-xs', 'onclick' => "return confirm('Are you sure you want to approve this Task?')"]) !!}

                  {!! Form::close() !!}

                  {!! Form::open(['route' => ['taskUsers.change', $taskUser->id, 2], 'method' => 'post']) !!}

                  {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Reject', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure you want to Reject this Task?')"]) !!}
                  {!! Form::close() !!}

                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
