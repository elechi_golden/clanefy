@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Task : {!! $task->title !!}
        </h1>
    </section>
    <div class="content">
      @include('flash::message')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('tasks.show_fields')

                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box box-primary">
          <h3  class="text-center">
              Task Completed by Users
          </h3>
            <div class="box-body">
                    @include('task_users.table-admin')
            </div>
        </div>
    </div>
@endsection
