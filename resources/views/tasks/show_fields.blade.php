<!-- Id Field -->


<!-- Title Field -->


<!-- Instruction Field -->
<div class="form-group">
    {!! Form::label('instruction', 'Instruction:') !!}
    <p>{!! $task->instruction !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('Created_by', 'Created By:') !!}
    <p> {{ $task->user['name']}}</p>
</div>

<!-- Reward Points Field -->
<div class="form-group">
    {!! Form::label('reward_points', 'Reward Points:') !!}
    <p>{!! $task->reward_points !!}</p>
</div>

<!-- Deleted At Field -->


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $task->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $task->updated_at !!}</p>
</div>
