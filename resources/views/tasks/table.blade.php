<table class="table table-responsive" id="tasks-table">
    <thead>
        <tr>
            <th>Tasks</th>
            <th>Pending</th>
            <th>Approved</th>

        <th>Point(s)</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
      <?php $count = 1 ?>
    @foreach($tasks as $task)

        <tr>
            <td>
              {{$count++ }}.
              <a href="{!! route('tasks.show', [$task->id]) !!}"><b> {!! $task->title !!}</b></a>
          <!--    <h3>Instruction</h3>
              <p>{!! $task->instruction !!}</p>
          -->
            </td>
            <td>{!! $task->tasks_pending_approval !!}</td>
            <td>{!! $task->tasks_approved !!}</td>
            <td>{!! $task->reward_points !!}</td>
            <td>

                <div class='btn-group'>
                  @if(Auth::user()->role_id == 1)
                  {!! Form::open(['route' => ['tasks.destroy', $task->id], 'method' => 'delete']) !!}

                    <a href="{!! route('tasks.edit', [$task->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    {!! Form::close() !!}
                  @endif
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
