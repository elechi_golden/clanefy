@extends('layouts.app')

@section('content')

    <div class="breadcome-area">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="breadcome-list">
                           <div class="row">
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                   <div class="breadcome-heading">
                                       <form role="search" class="sr-input-func">
                                           <input type="text" placeholder="Search..." class="search-int form-control">
                                           <a href="#"><i class="fa fa-search"></i></a>
                                       </form>
                                   </div>
                               </div>
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                   <ul class="breadcome-menu">
                                       <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                       </li>
                                       <li><span class="bread-blod">Manage Users</span>
                                       </li>
                                       <h1 class="pull-right">
                                          <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Add New</a>
                                       </h1>
                                   </ul>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    <div class="container-fluid">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('users.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
