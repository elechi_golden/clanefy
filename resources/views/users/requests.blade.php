@extends('layouts.app')

@section('content')
<div class="breadcome-area">
       <div class="container-fluid">
           <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="breadcome-list">
                       <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <div class="breadcome-heading">
                                   <form role="search" class="sr-input-func">
                                       <input type="text" placeholder="Search..." class="search-int form-control">
                                       <a href="#"><i class="fa fa-search"></i></a>
                                   </form>
                               </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <ul class="breadcome-menu">
                                   <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                   </li>
                                   <li><span class="bread-blod">Connection Request</span>
                                   </li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>




    <div class="contacts-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                  @foreach ($requests as $request)
                      @if(!empty($request))
                          <?php
                              $user = App\User::find($request->requester);
                          ?>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="hpanel hblue contact-panel contact-panel-cs responsive-mg-b-30">
                            <div class="panel-body custom-panel-jw">
                                <div class="social-media-in">
                                  @if ($user->role_id == 5)
                                    <a href="#"><i class="fas fa-crown"></i></a>

                                  @else($user->role_id != 5)
                                  <a href="#"><i class="fas fa-user-circle"></i></a>

                                  @endif
                                </div>
                                <img alt="avatar" class="img-circle m-b" height="100px" width="100px" src="{{asset('uploads/avatars')}}/{{$user->avatar}}">
                                <h3><a href="">{{$user->name}}</a></h3>
                              <p>{{$user->email}}</p>
                                <p class="all-pro-ad">Calabar, CRS</p>
                                <p>
                                    {{$user->bio}}
                                </p>
                            </div>
                          <hr>
                            <div>

                                        <p class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px">Profile</p>
                                        <a class="btn btn-custon-rounded-two widget-btn-1 btn-sm" style="margin-top: -10px;margin-bottom: 5px" href="/connect/{{$request->id}}/accept">Connect</a>
                                        @else
                                          
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="caption pro-sl-hd">
                                                    <span class="caption-subject"><b><h3>There are No connection Request at this time</h3></b></span>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                          </div>

                        </div>
                    </div>



                </div>
              </div>
            </div>





@endsection
