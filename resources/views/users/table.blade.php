<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
          <th>User Type</th>
            <th>Name</th>
        <th>Email</th>
        <th>Email Verified At</th>

        <th>Referred By</th>
        <th>No Of Refs</th>
        <th>Ref Level Id</th>

            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
          
            <td><a href="/profile/{{$user->id}}">{!! $user->name !!}</a></td>
            <td>{!! $user->email !!}</td>
            <td>{!! $user->email_verified_at !!}</td>
            <td>{!! $user->referred_by !!}</td>
            <td>{!! $user->no_of_refs !!}</td>
            <td>{!! $user->ref_level_id !!}</td>


            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
