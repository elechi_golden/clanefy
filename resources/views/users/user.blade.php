@extends('layouts.app')

@section('content')


    <div class="breadcome-area">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="breadcome-list">
                           <div class="row">
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                   <div class="breadcome-heading">
                                       <form role="search" class="sr-input-func">
                                           <input type="text" placeholder="Search..." class="search-int form-control">
                                           <a href="#"><i class="fa fa-search"></i></a>
                                       </form>
                                   </div>
                               </div>
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                   <ul class="breadcome-menu">
                                       <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                       </li>
                                       <li><span class="bread-blod">User</span>
                                       </li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>

    <div class="contacts-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="hpanel hblue contact-panel contact-panel-cs responsive-mg-b-30">
                            <div class="panel-body custom-panel-jw">
                                <div class="social-media-in">
                                  @if ($user->role_id == 5)
                                    <a href="#"><i class="fas fa-crown"></i></a>

                                  @else($user->role_id != 5)
                                  <a href="#"><i class="fas fa-user-circle"></i></a>

                                  @endif
                                </div>
                                <img alt="logo" class="img-circle m-b" height="100px" width="100px" src="{{ asset ('uploads/avatars')}}/{{$user->avatar}}">
                                <h3><a href="">{{$user->name}}</a></h3>
                              <p>{{$user->email}}</p>
                                <p class="all-pro-ad">{{$user->address}}</p>
                                <p>
                                    {{$user->bio}}
                                </p>
                            </div>
                          <hr>
                            <div>
                            @if(Auth::check() && Auth::id() != $user->id)
                                    @if (Auth::user()->hasfriend($user->id) == 'requested')
                                        <p class="btn btn-success" style="margin-top: -10px;margin-bottom: 5px">Connection Sent</p>
                                    @elseif (Auth::user()->hasfriend($user->id) == 'friends')
                                        <p class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px">Connected</p>
                                    @else
                                        <a class="btn btn-custon-rounded-two widget-btn-1 btn-sm" style="margin-top: -10px;margin-bottom: 5px" href="/connect/add/{{$user->id}}">Add Friend</a>
                                    @endif
                            @endif
                          </div>

                        </div>
                    </div>



                </div>
              </div>
            </div>





@endsection
