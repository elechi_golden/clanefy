<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone No:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- DOB Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dob', 'Date Of Birth:') !!}

</div>
<!-- gender Field -->

<div class="form-group">
    <select class="form-control">
<option>Select Gender</option>
<option>Male</option>
<option>Female</option>
</select>
</div>
<!-- address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<!-- Bio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bio', 'bio:') !!}
    {!! Form::textarea('bio', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
