@extends('layouts.app')
@section('content')
<div class="payment-cart-pro mg-b-30">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                       <div class="card payment-card responsive-mg-b-30">
                           <div class="payment-inner-pro">
                               <i class="fas fa-wallet" aria-hidden="true"></i>
                               <h5>Wallet Summary</h5>
                               <div class="row m-t-10">

                                   <div class="col-sm-6 col-md-6 col-sm-6 col-xs-6">
                                       <strong class="m-r-5">Access Fee :</strong> $5<br />
                                       <strong class="m-r-5"> Last withdrawal </strong>$6<br/>
                                       <strong class="m-r-5">Available For withdrawal :</strong> $10
                                   </div>
                               </div>


                           </div>
                       </div>
                   </div>
                   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                       <div class="card payment-card responsive-mg-b-30">
                           <div class="payment-inner-pro">
                               <i class="fas fa-thumbs-up" aria-hidden="true"></i>
                               <h5>Earn By Cheer</h5>
                               <div class="row m-t-10">

                                   <div class="col-sm-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                       <strong class="m-r-5"></strong>


                                       <br />

                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                       <div class="card payment-card tb-sm-res-d-n dk-res-t-d-n">
                           <div class="payment-inner-pro">
                               <i class="fas fa-tasks" aria-hidden="true"></i>
                               <h5>Task Earning</h5>
                               <div class="row m-t-10">

                                   <div class="col-sm-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                       <strong class="m-r-5"></strong>


                                       <?php $totalPointEarned = 0;
                                             $totalPointPending = 0;
                                        ?>
                                       @foreach($taskUsers as $taskUser)
                                         @if($taskUser->status == 1)
                                           <?php
                                             $totalPointEarned = $taskUser->reward_points + $totalPointEarned;

                                            ?>
                                         @endif

                                         @if($taskUser->status == 0)
                                         <?php
                                           $totalPointPending = $taskUser->reward_points + $totalPointPending;

                                          ?>

                                         @endif

                                       @endforeach
                                           Pending Points: {{$totalPointPending}}  | Points Earned: {{$totalPointEarned}}






                                       <br />

                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- Single pro tab review Start-->
       <div class="single-pro-review-area mt-t-30 mg-b-15">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="product-payment-inner-st">
                           <ul id="myTab4" class="tab-review-design">
                             <li class="active"><a href="#netbanking">Bank Details</a></li>
                               <li><a href="#account"><i class="fas fa-plus"></i> Account Details</a></li>
                               <li><a href="#transac"> Transactions</a></li>
                               <li><a href="#cashout">Cashout</a></li>


                           </ul>
                           <div id="myTabContent" class="tab-content custom-product-edit">
                             <div class="product-tab-list tab-pane fade active in" id="netbanking">
                                 <div class="row">
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                         <div class="review-content-section">
                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                 <div class="review-content-section">

                                                   <?php $account = \App\Models\User::findOrFail(auth()->user()->id)->account;
                                                   $user_id = empty($account)? auth()->user()->id : $account->user_id; ?>

                                                  <strong class="m-r-5">Account Number :</strong> {{ empty($account->account_num)? 'Nil' : $account->account_num }}<br />
                                                  <strong class="m-r-5"> Account Name :</strong>{{ empty($account->account_name)? 'Nil' : $account->account_name }}<br/>
                                                <strong class="m-r-5">Bank :</strong> {{ empty($account->bank)? 'Nil' : $account->bank }}
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                               <div class="product-tab-list tab-pane fade " id="account">
                                   <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                           <div class="review-content-section">
                                               <div class="demo-container">
                                                   <div class="card-wrapper"></div>
                                                   <form class="payment-form mg-t-30" action="wallet/{{$user_id}}" method="post">
                                                     @if(!empty($account))
                                                      @method('PATCH')
                                                    @endif
                                                     {!! csrf_field() !!}
                                                       <div class="form-group">
                                                           <input name="number" type="tel" value="{{ empty($account->account_num)? '' : $account->account_num }}" class="form-control" placeholder="Account Number">
                                                       </div>
                                                       <div class="form-group">
                                                           <input name="name" type="text" value="{{ empty($account->account_name)? '' : $account->account_name }}" class="form-control" placeholder="Account Name">
                                                       </div>

                                                       <div class="review-content-section">
                                                         <select class="form-control mg-b-15" name="type">
                                                           <option>{{ empty($account->account_type)? 'Account Type' : $account->account_type }}</option>
                                                          <option>Savings</option>
                                                          <option>Current</option>

                                                         </select>
                                                           <select class="form-control mg-b-15" name="bank">
                                       <option>{{ empty($account->bank)? 'Select Bank' : $account->bank }}</option>
                                       <option>Access Bank Plc </option>
                                       <option>Citibank Nigeria Limited </option>
                                       <option>Diamond Bank Plc </option>
                                       <option>Ecobank Nigeria Plc </option>
                                       <option>Fidelity Bank Plc </option>
                                       <option>FIRST BANK NIGERIA LIMITED</option>
                                       <option>First City Monument Bank Plc </option>
                                       <option>Guaranty Trust Bank Plc </option>
                                       <option>Heritage Banking Company Ltd. </option>
                                       <option>Key Stone Bank </option>
                                       <option>Polaris Bank  </option>
                                       <option>	Providus Bank  </option>
                                       <option> Stanbic IBTC Bank Ltd.  </option>
                                       <option>Standard Chartered Bank Nigeria Ltd.  </option>
                                       <option> Sterling Bank Plc  </option>
                                       <option>SunTrust Bank Nigeria Limited  </option>
                                       <option>Union Bank of Nigeria Plc  </option>
                                       <option>United Bank For Africa Plc  </option>
                                       <option>Unity Bank Plc   </option>
                                       <option>Wema Bank Plc   </option>
                                       <option>Zenith Bank Plc</option>
                                     </select>


                                                       </div>
                                                        @if(!empty($account))
                                                         <div class="text-center credit-card-custom">
                                                             <input type="submit" class="btn btn-primary waves-effect waves-light" value="UPDATE"/>
                                                         </div>
                                                        @else
                                                         <div class="text-center credit-card-custom">
                                                             <input type="submit" class="btn btn-primary waves-effect waves-light" value="ADD"/>
                                                         </div>
                                                        @endif
                                                       @if($errors->any())
                                                        <div class="col-md-12 alert alert-danger">
                                                          @foreach($errors->all() as $err)
                                                           <li>{{ $err }}</li>
                                                          @endforeach
                                                        </div>
                                                      @endif
                                                       @if(!empty(session('error')))
                                                        <div class="col-md-12 alert alert-danger">
                                                          @foreach(session('error') as $errors)
                                                           <li>{{ $errors }}</li>
                                                          @endforeach
                                                        </div>
                                                      @endif
                                                   </form>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="product-tab-list tab-pane fade" id="transac">
                                   <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                           <div class="review-content-section">
                                               <div class="row">
                                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <?php $user_transactions = \App\Cashout::where('user_id',auth()->user()->id)->get(); ?>
                                                       <div class="table-responsive">
                                                         <?php if(count($user_transactions) != 0): ?>
                                                          <table class="table table-bordered">
                                                            <thead>
                                                              <th>Account name</th>
                                                              <th>Account number</th>
                                                              <th>Amount withrawal</th>
                                                              <th>status</th>
                                                            </thead>
                                                            <tbody>
                                                              <?php foreach($user_transactions as $transaction): ?>
                                                              <tr>
                                                                <td>{{ $transaction->account_name }}</td>
                                                                <td>{{ $transaction->account_num }}</td>
                                                                <td>${{ $transaction->amount }}</td>
                                                                <td>
                                                                  @if($transaction->status == 1)
                                                                  <a href="#" class="btn btn-info">success</a>
                                                                  @else
                                                                  <a href="#" class="btn btn-danger">Pending</a>
                                                                  @endif
                                                                </td>
                                                              </tr>
                                                            <?php endforeach ?>
                                                            </tbody>
                                                          </table>
                                                        <?php endif ?>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <?php $userPoints = \App\models\Earning::where('user_id',auth()->user()->id)->get(); ?>
                               <?php if(count($userPoints) != '0'){
                                 foreach($userPoints as $userPoint){
                                   $totalPoints = $userPoint->points;
                                   $user_balance = $userPoint->earnings;
                                 }
                               }
                               $standard_point = \App\Point::first();
                               if(!empty($standard_point)){
                                   $points = $standard_point->points;
                                   $max = $standard_point->max;
                                   $min = $standard_point->min;
                               }
                               ?>
                               <div class="product-tab-list tab-pane fade" id="cashout">
                                   <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                         <div class="col-md-12">
                                           <div class="col-md-6">
                                             <p class="alert alert-info"><strong>Please endeavour to cross check your details before cashing out</strong></p>
                                           </div>
                                         </div>
                                           <div class="review-content-section">
                                             <strong class="m-r-5">Min withdrawal :</strong> {{ empty($min)? 'Nil' : "$$min" }}<br />
                                             <strong class="m-r-5"> Max withdrawal : </strong> {{ empty($max)? 'Nil' : "$$max" }}<br/>
                                           <strong class="m-r-5">Available For Withdrawal :</strong>${{ (empty($user_balance))? '0' : $user_balance }} </br>
                                           <form action="wallet/{{ auth()->user()->id }}/cashout" method="post">
                                             {{ csrf_field() }}
                                             <input class="form-control mg-b-15" name="amount" placeholder="Enter Amount">

                                             <button type="submit" class="btn btn-primary waves-effect waves-light">Cash Out</button>
                                             @include('error')
                                             @if(!empty(session('user_error')))
                                              <div class="alert alert-warning">
                                                @foreach(session('user_error') as $error)
                                                  <li>{{ $error }}</li>
                                                @endforeach
                                              </div>
                                            @endif
                                           </form>
                                           </div>
                                       </div>
                                   </div>
                               </div>


                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                        <div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header header-color-modal bg-color-1">
                                        <h4 class="modal-title">Cash Out Details</h4>
                                        <div class="modal-close-area modal-close-df">
                                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                        </div>
                                    </div>
                                    <div class="modal-body">

                                      <strong class="m-r-5">Account Number :</strong> 2199910<br />
                                      <strong class="m-r-5"> Account Name :</strong>Moses Golden<br/>
                                    <strong class="m-r-5">Bank :</strong> FIRST BANK OF NIGERIA PLC </br>
                                    <strong class="m-r-s-5"> Amount : </strong> $200
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" href="#">Cancel</a>
                                        <a href="#">Process</a>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                  </div>
       @endsection
