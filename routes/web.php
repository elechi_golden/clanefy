<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('tickets', 'TicketsController@index');
    Route::post('close_ticket/{ticket_id}', 'TicketsController@close');
});

// Route::get('/test', function(){
//     return Auth::user()->test();
// });

// wallet View
Route::group(['middleware'=> ['auth']],function(){
Route::get('wallet', 'EarningController@index');
Route::get('wallet', 'EarningController@index')->name('wallet');
Route::post('wallet/{id}','AccountController@store');
Route::patch('wallet/{id}','AccountController@update');
Route::post('wallet/{id}/cashout','AccountController@cashout');
});

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('/unread', ['as' => 'messages.unread', 'uses' => 'MessagesController@unread']); // ajax + Pusher
    Route::get('/create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    Route::get('{id}/read', ['as' => 'messages.read', 'uses' => 'MessagesController@read']); // ajax + Pusher
});

Route::group(['middleware'=> ['auth']],function(){
Route::get('/inhouse/admin', 'AdminController@admin');
Route::get('/inhouse/resetpw', 'AdminController@pwreset');

Route::get('/inhouse/token_generator', 'AdminController@token');
Route::post('/inhouse/generate', 'AdminController@generate_token');
Route::get('/inhouse/users', 'AdminController@users');
Route::get('/inhouse/create_task/{id}', 'AdminController@createTask');
Route::post('/inhouse/assign_task/{id}', 'AdminController@assignTask');
Route::get('/inhouse/task_settings', 'AdminController@taskSettings');
Route::post('/inhouse/task_settings/set', 'AdminController@set');
Route::patch('/inhouse/task_settings/update/{id}', 'AdminController@updatePoint');
Route::get('/inhouse/approve', 'AdminController@approve');
Route::get('/inhouse/cashout', 'AdminController@cashoutView');
Route::patch('/inhouse/cashout/{transaction_id}', 'AdminController@cashout');
Route::get('/inhouse/audit', 'AdminController@audit_view');
Route::patch('/inhouse/approve/{id}/task/{task_id}', 'AdminController@approveTask');
Route::resource('/inhouse/reflevels', 'ReflevelController');
Route::resource('/inhouse/tasks', 'TaskController');
});



Route::get('/logout', 'AdminController@logout');

Route::match(['get','post'],'/inhouse', 'AdminController@login');

Route::get('/front', 'HomeController@index')->name('home');


Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->middleware('verified');

Route::group(['middleware'=> ['auth']],function(){

Route::resource('users', 'UserController');

Route::post('/home/upgrade', 'HomeController@upgrade');

Route::resource('roles', 'RoleController');

Route::resource('refCategories', 'RefCategoryController');
});
Route::get('refs/{user_id}/{ref_category_id}','RefCategoryController@refs')->name('refs.user')->middleware('guest');

Route::group(['middleware'=> ['auth']],function(){


Route::resource('taskUsers', 'TaskUserController');

Route::post('taskUsers/{taskUser_id}/{status_value}', 'TaskUserController@change')->name('taskUsers.change');

Route::get('new_ticket', 'TicketsController@create');

Route::post('new_ticket', 'TicketsController@store');

Route::get('my_tickets', 'TicketsController@userTickets');

Route::get('tickets/{ticket_id}', 'TicketsController@show');

Route::post('comment', 'CommentsController@postComment');

 Route::resource('groups', 'GroupController');

 Route::resource('conversations', 'ConversationController');
});

//
Route::group(['middleware'=> ['auth']],function(){
Route::get('/profile/{id}', 'UserController@user');
Route::get('/myprofile', 'UserController@profile');
Route::post('/myprofile', 'UserController@update_avatar');
});

Route::group(['prefix' => 'connect', 'middleware' => 'auth'], function() {
    Route::get('/', 'FriendshipsController@index');
    Route::get('/add/{id}', 'FriendshipsController@store');
    Route::get('/requests', 'FriendshipsController@create');
    Route::get('/{id}/accept', 'FriendshipsController@update');
    Route::get('/{id}/delete', 'FriendshipsController@destroy');
});

Route::group(['prefix' => 'post', 'middleware' => 'auth'], function() {
    Route::get('/', 'PostController@index');
    Route::get('/{id}', 'PostController@show');

    Route::post('/store', 'PostController@store');

    Route::get('/{id}/edit', 'PostController@edit');

    Route::patch('/{id}/update', 'PostController@update');
    Route::delete('/{id}/delete', 'PostController@destroy');
});

Route::group(['prefix' => 'comment', 'middleware' => 'auth'], function() {
    Route::get('/', 'PostCommentController@index');
    Route::get('/{id}', 'PostCommentController@show');

    Route::post('/store', 'PostCommentController@store');

    Route::get('/{id}/edit', 'PostCommentController@edit');

    Route::patch('/{id}/update', 'PostCommentController@update');
    Route::delete('/{id}/delete', 'PostCommentController@destroy');
});


Route::group(['prefix' => 'like', 'middleware' => 'auth'], function() {
    //Route::get('/', 'PostCommentLikeController@index');
    //Route::get('/{id}', 'PostLikeController@show');

    Route::get('/store/{post_id}', 'PostLikeController@store');

    //Route::get('/{id}/edit', 'PostLikeController@edit');

    //Route::patch('/{id}/update', 'PostLikeController@update');
    Route::get('/{id}/delete', 'PostLikeController@destroy');
});

Route::group(['prefix' => 'reply', 'middleware' => 'auth'], function() {
    Route::get('/', 'ReplyController@index');
    Route::get('/{id}', 'ReplyController@show');

    Route::post('/store', 'ReplyController@store');

    Route::get('/{id}/edit', 'ReplyController@edit');

    Route::patch('/{id}/update', 'ReplyController@update');
    Route::delete('/{id}/delete', 'ReplyController@destroy');
});


  Route::get('/chat', 'ChatController@index')->middleware('auth')->name('chat.index');
  Route::get('/chat/{id}', 'ChatController@show')->middleware('auth')->name('chat.show');
  Route::post('/chat/getChat/{id}', 'ChatController@getChat')->middleware('auth');

Route::group(['prefix' => 'notification', 'middleware' => 'auth'], function() {
    Route::get('/', 'PostsNotificationController@index');
    Route::get('/{id}', 'PostsNotificationController@show');

    // Route::post('/store', 'PostsNotificationController@store');

    // Route::get('/{id}/edit', 'PostsNotificationController@edit');

    Route::patch('/{id}/update', 'PostsNotificationController@update');

    // Route::delete('/{id}/delete', 'PostsNotificationController@destroy');
});
